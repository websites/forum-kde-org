/**
*
* @package google_translation_phpBB3
* @version $Id: gtrans.js 3171 2009-06-19 18:29 sayakb $
* @copyright (c) 2009 Sayak Banerjee
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

var loaded = false;
loaded = google.load("language", "1");

function translate(post_id)
{
	if ( loaded == false )
	{
		document.getElementById("transto" + post_id).style.display = 'none';
		document.getElementById("selector" + post_id).style.display = 'none';
		document.getElementById("errl" + post_id).style.display = 'inline';
		return false;
	}
	var lang = document.getElementById('s_selector' + post_id).value;
	if ( lang == 'xx' ) return false;
	var id_article = "article" + post_id;
	var id_original = "original" + post_id;
	var source = document.getElementById(id_article).innerHTML;
	var len = source.length;
	var words = 1000;
	var sourcelang = "en";
	var first = 1;
	document.getElementById("trango" + post_id).style.display = 'inline';
	document.getElementById(id_original).innerHTML = document.getElementById(id_article).innerHTML;
	for(i=0; i<=(len/words); i++)
	{
		google.language.translate (source.substr(i*words, words), "", lang, function (result)
		{
			if (!result.error)
			{
				if ( first )
				{
					document.getElementById("steng" + post_id).style.display = 'inline';
					document.getElementById("trango" + post_id).style.display = 'none';
					document.getElementById(id_article).innerHTML="";
					first = 0;
					if(len > 1000)
					{
						document.getElementById("notify" + post_id).style.display = 'inline';
					}
				}
				document.getElementById(id_article).innerHTML = document.getElementById(id_article).innerHTML + result.translation;
			}
		});
	}

	document.getElementById("selector" + post_id).style.display = 'none';
	return false;
}

function showGTMenu(post_id)
{
	document.getElementById("transto" + post_id).style.display = 'none';
	document.getElementById("selector" + post_id).style.display = 'inline';
	return false;
}

function hideGTMenu(post_id)
{
	document.getElementById("transto" + post_id).style.display = 'inline';
	document.getElementById("selector" + post_id).style.display = 'none';
	document.getElementById("s_selector" + post_id).value = 'xx';
	return false;
}

function ack_err(post_id)
{
	document.getElementById("errl" + post_id).style.display = "none";
	document.getElementById("transto" + post_id).style.display = "inline";
	return false;
}

function original(post_id)
{
	var id_original = "original" + post_id;
	var id_article = "article" + post_id;

	document.getElementById(id_article).innerHTML = document.getElementById(id_original).innerHTML;
	document.getElementById("steng" + post_id).style.display = 'none';
	document.getElementById("transto" + post_id).style.display = 'inline';
	document.getElementById("notify_m" + post_id).style.display = 'none';
	document.getElementById("notify_o" + post_id).style.display = 'none';
	document.getElementById("notify" + post_id).style.display = 'none';
	document.getElementById("s_selector" + post_id).value = 'xx';
	document.getElementById("notify_m" + post_id).innerHTML = "One moment...";
	return false;
}

function showInfo(post_id)
{
	var notice = 'This post exceeds the maximum message size allowed by Google. As a result, the document is split into pieces before being translated. Splitting the posts can cause discontinuity in translation and broken HTML tags. If such a situation arises, please use an external translation software instead.';
	var lang = document.getElementById("s_selector" + post_id).value;
	if (!lang) lang="en";
	document.getElementById("notify_m" + post_id).style.display = 'block';
	google.language.translate (notice, "en", lang, function (result)
	{
		if (!result.error)
		{
			document.getElementById("notify_m" + post_id).innerHTML = result.translation;
		}
	});
	document.getElementById("notify" + post_id).style.display = 'none';
	document.getElementById("notify_o" + post_id).style.display = 'inline';
	return false;
}

function hideInfo(post_id)
{
	document.getElementById("notify_m" + post_id).innerHTML = "One moment...";
	document.getElementById("notify_m" + post_id).style.display = 'none';
	document.getElementById("notify" + post_id).style.display = 'inline';
	document.getElementById("notify_o" + post_id).style.display = 'none';
	return false;
}
/**
*
* @package javascript_phpBB3
* @version $Id: kdeforum.js 1003 2009-09-03 23:47 sayakb $
* @copyright (c) 2010 Sayak Banerjee
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/*
* This file is a unified JS placeholder.
* Remember to add the package name when adding it's corresponding js
*/

/*
* Function to hide new PM notification bar
* Package: new_pm_notify_phpbb3
*/
function hideNewPm()
{
	try
	{
		var url = window.location.toString();
		var sep = '&';
		
		if (url.indexOf('?') == -1)
		{
			sep = '?';
		}
		
		url += sep + 'pm_notify=hide';
	
		var src = $('.pmnotify_close_button').attr('src');
		var len = src.lastIndexOf('/') + 1;
		src = src.substr(0, len) + 'spinner_small.gif';

		$('.pmnotify_close_button').attr('src', src);
		
		$.get(url, function() {
			$('.pmnotify').animate({
				top: '-40px'
			}, 'slow', function() {
				$('.pmnotify').css('display', 'none');
			});
		});
		
		return false;
	}
	catch(e)
	{
		return false;
	}
}

/*
* Hide the intro box
* Package: intro_box_phpbb3
*/
function hideIntro(hideLink)
{
	try
	{
		$('#introbox').fadeTo(600, 0, function() {
			$('#introbox').slideUp(400, function() {
				$.get(hideLink);
			});
		});
	}
	catch(e)
	{
		console.log("Error: " + e.message);
	}
}

/*
* Method to handle the identity account choose username page
*/
function identityChooseUname()
{
	try
	{
		if ($('#choose_auto').length > 0)
		{
			if ($('#choose_new').is(':checked'))
			{
				$('.choose_existing').hide();
			}
			else if ($('#choose_existing').is(':checked'))
			{
				$('.choose_new').hide();
			}
			else
			{
				$('.choose_new, .choose_existing').hide();
			}

			$('#choose_auto').click(function() {
				$('.choose_new, .choose_existing').hide();
			});

			$('#choose_new').click(function() {
				$('.choose_existing').hide();
				$('.choose_new').show();
			});

			$('#choose_existing').click(function() {
				$('.choose_existing').show();
				$('.choose_new').hide();
			});
		}
	}
	catch(e)
	{
		console.log("Error: " + e.message);
	}
}

/*
* Function to get a cookie value
* Package: kdeforum_general_phpbb3
*/

function populateLayoutNav()
{
	try
	{
		if ($('.root h2, th.big').length > 0 && !is_khtml)
		{
			$('.root h2, th.big').each(function() {
				if (!$(this).hasClass('layout-nav-skip'))
				{
					var hashText = getSafeString($(this).text());
					var id = $(this).attr('id');

					if (typeof(id) == 'undefined' || id.length == 0)
					{
						id = hashText;
						$(this).attr('id', id);
					}

					$('#layout-nav-menu').append(
						'<li><a href="#' + id + '" class="smooth">' +
						'<i class="icon-share-alt icon-black"></i>&nbsp;' +
						$(this).text().trim() + '</a></li>'
					);
				}
			});

			$('#layout-nav-caret').css('display', 'inline-table');
		} else {
            $('#layout-nav').css('marginRight', -10);
            $('#layout-nav-menu, #layout-nav-caret').remove();
        }
	}
	catch(e)
	{
		console.log("Error: " + e.message);
	}
}

/*
* Make collapsible forum categories
* Package: kdeforum_general_phpbb3
*/
function generateCategories()
{
	try
	{
		$('.category-toggle').click(function() {
			var src = $(this).attr('src');
			var new_src;
			var is_collapsed = src.indexOf('expand') != -1;
			var category = $(this).attr('data-category');
			var cookie_data = getCookie('phpbb_categories_collapsed').split(',');
			var pos = $.inArray(category, cookie_data);

			if (is_collapsed)
			{
				new_src = src.replace('expand', 'collapse');
				$('tr[data-category=' + category + ']').fadeIn(150);

				cookie_data.splice(pos, 1);
			}
			else
			{
				new_src = src.replace('collapse', 'expand');
				$('tr[data-category=' + category + ']').fadeOut(150);

				if (pos == -1)
				{
					cookie_data.push(category);
				}
			}

			$(this).attr('src', new_src);
			setCookie('phpbb_categories_collapsed', cookie_data.join(','), 365);
		});
	}
	catch(e)
	{
		console.log("Error: " + e.message);
	}
}

/*
* Collapse categories on startup
* Package: kdeforum_general_phpbb3
*/
function collapseCategories()
{
	try
	{
		var cookie_data = getCookie('phpbb_categories_collapsed').split(',');

		for (idx in cookie_data)
		{
			try
			{
				var category = cookie_data[idx];

				if (category.length > 0)
				{
					var toggle = $('img[data-category=' + category + ']').attr('src');

					$('tr[data-category=' + category + ']').hide();
					$('img[data-category=' + category + ']').attr('src', toggle.replace('collapse', 'expand'));
				}
			}
			catch(e) { }
		}
	}
	catch(e)
	{
		console.log("Error: " + e.message);
	}
}

/*
* Returns a ID friendly string for a text
* Package: kdeforum_general_phpbb3
*/
function getSafeString(text)
{
	var safe = text.replace(/ /g, '-').replace(/[^A-Za-z0-9\-']/g, '');

	while (safe.indexOf('--') != -1)
		safe = safe.replace(/--/g, '-');

	return escape(safe.toLowerCase());
}

/*
* Initialize the topic preview modal
* Package: topic_preview_phpbb3
*/
function initTopicPreview() {
	var popup_timer;
	var link_timer;
	var animated = false;
	var mouseX, mouseY;

	// Track mouse, for enhancing hover
	$(document).mousemove(function(e) {
		mouseX = e.pageX;
		mouseY = e.pageY;
	});

	// Link hover and click events to the preview link
	$('.topic-preview')
		.hover(function() {
			if (!animated) {
				id = $(this).attr('data-id');
				url = $(this).attr('data-target');

				link_top = $(this).position().top;
				link_left = $(this).position().left;
				link_bottom = link_top + $(this).innerHeight();
				link_right = link_left + $(this).innerWidth();


				// Show a timer
				$(this).children('i').replaceWith('<span id="topic-preview-timer"></span>');
				$('#topic-preview-timer').pietimer({
					colour: '#0068C6',
					seconds: 0.8,
					width: 11,
					height: 11
				}, function() {
					$('#topic-preview-timer').replaceWith('<i class="icon-zoom-in"></i>');
				});

				clearTimeout(popup_timer);
				popup_timer = setTimeout(function() {
					if (mouseX >= link_left && mouseX <= link_right && mouseY >= link_top && mouseY <= link_bottom) {
						showTopicPreview(id, url);
					}
				}, 1000);
			}
		},
		function() {
			clearTimeout(popup_timer);

			// Hide the timer
			$(this).children('#topic-preview-timer').replaceWith('<i class="icon-zoom-in"></i>');
		})
		.click(function() {
			if (!animated) {
				id = $(this).attr('data-id');
				url = $(this).attr('data-target');
				
				clearTimeout(popup_timer);
				showTopicPreview(id, url);
				
				return false;
			}
		});

	// Detect if modal is shown or is being shown
	$('#topic-preview')
		.on('show', function() {
            $('body').css({
                overflow: 'hidden',
                paddingRight: 15
            });
            
            $('.navbar-bottom .navbar-inner').css({
                marginRight: -15,
                paddingRight: 50
            });
            
            animated = true;
        })
		.on('shown', function() {
            animated = false;
        })
        .on('hidden', function() {
            $('body').css({
                overflow: 'auto',
                paddingRight: 0
            });

            $('.navbar-bottom .navbar-inner').css({
                marginRight: 0,
                paddingRight: 20
            });

            $('.modal-backdrop').remove();
        });
}

/*
* Show the topic preview modal
* Package: topic_preview_phpbb3
*/
function showTopicPreview(id, url) {
	try {
		if (typeof(id) != 'undefined' && typeof(url) != 'undefined') {
			// Show the modal
			$('#topic-preview')
				.modal('show')
				.css({
					maxHeight: viewportHeight,
					top: 320
				});
			
			// Make a nice scrollbar
			$('#topic-preview .modal-body')
				.slimScroll({
					height: viewportHeight - 240,
					color: '#0068c6',
					opacity: 0.6
				})
				.css('maxHeight', viewportHeight - 240);

			// Give it a title
			var topic_title = $('h4[data-preview=' + id + ']').text().replace(/\s\s+/g, ' ').trim();

			if (topic_title.length > 45) {
				topic_title = topic_title.substr(0, 45) + '...';
			}
			
			$('#topic-preview-title').html('<h3>' + topic_title + '</h3>');

			// Show loading message
			$('#topic-preview-link').attr('href', url);
			$('#topic-preview-body').html(
				'<div class="align-center">' +
					'<button class="btn loading" disabled>' +
						'<i class="icon-time icon-black"></i>&nbsp;' +
						lang_loading +
					'</button>' +
				'</div>'
			);

			// Get the topic data
			$.get(url + '&tp=1', function(data) {
				$('#topic-preview-body').html(data);
				$('#topic-preview').css('top', 320);

				// Set the form action
				$('#kb-container').attr('action', url);

				// Set up brainstorm again
				if (window.brainstormStartup) {
					brainstormStartup();
				}
			}).error(void(0));
		}
	} catch(e) { }
}

/*
* Function to get a cookie value
* Package: kdeforum_general_phpbb3
*/
function getCookie(c_name)
{
	try
	{
		if (document.cookie.length>0)
		{
			c_start = document.cookie.indexOf(c_name + "=");

			if (c_start != -1)
			{
				c_start = c_start + c_name.length + 1;
				c_end = document.cookie.indexOf(";", c_start);

				if (c_end == -1)
				{
					c_end = document.cookie.length;
				}

				return unescape(document.cookie.substring(c_start, c_end));
			}
		}

		return "";
	}
	catch(e)
	{
		console.log("Error: " + e.message);
		return "";
	}
}

/*
* Function to set a cookie value
* Package: kdeforum_general_phpbb3
*/
function setCookie(c_name, value, expiredays)
{
	try
	{
		var exdate=new Date();

		exdate.setDate(exdate.getDate() + expiredays);
		document.cookie = c_name + "=" + escape(value) +
						((expiredays == null) ? "" : ";expires=" + exdate.toUTCString());
	}
	catch(e)
	{
		console.log("Error: " + e.message);
	}
}

/*
* Set default button for a form
* Package: kdeforum_general_phpbb3
*/
function setDefaultButton()
{
	$('form > input').keypress(function (e) {
		if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13))
		{
			var $default = $(this).parent('form').find('input[type=submit]').first();

			if ($default.length > 0)
			{
				$default.click();
				return false;
			}
		}

		return true;
	});
}

/*
* Startup function
* Package: kdeforum_general_phpbb3
*/
function kdeForumStartup()
{
	// Initialize guided posting
	// Package guided_posting_phpbb3
	if (window.guidedStartup)
		guidedStartup();
	
	// Initialize identity link page
	// Package identity_auth_phpbb3
	if (window.identityChooseUname)
		identityChooseUname();

	// Perform KDE brainstorm startup
	// Package simple_brainstorm_phpbb3
	if (window.brainstormStartup)
		brainstormStartup();

	// Populate layout navigation links
	// Package kdeforum_general_phpbb3
	if (window.populateLayoutNav)
		populateLayoutNav();

	// Make collapsible forum categories
	// Package kdeforum_general_phpbb3
	if (window.generateCategories)
		generateCategories();

	// Collapse categories on startup
	// Package kdeforum_general_phpbb3
	if (window.collapseCategories)
		collapseCategories();

	// Collapse categories on startup
	// Package topic_preview_phpbb3
	if (window.initTopicPreview)
		initTopicPreview();

	// Set default button for a form
	// Package kdeforum_general_phpbb3
	if (window.setDefaultButton)
		setDefaultButton();
}
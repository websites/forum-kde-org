/**
*
* @package guided_posting_phpBB3
* @version $Id: guided.js sayakb $
* @copyright (c) 2010 Sayak Banerjee
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

// Hook all events for guided apps filtering
function guidedStartup()
{
	$(".guided-filter").keyup(function() {
		var key = $(this).val().trim().toLowerCase();
		
		$(".guided-app").each(function() {
			var appName = $(this).html().trim().toLowerCase();
			var target = $(this).attr("data-target");

			if (appName.indexOf(key) == -1) {
				$("." + target)
					.stop(true, true)
					.fadeOut();
			} else {
				$("." + target)
					.stop(true, true)
					.fadeIn();
			}
		});
	});

	$(".guided-apps").slimScroll({
		width:'305px',
		height:'305px',
		color: '#0068c6',
		opacity: 0.6
	});
}
/**
*
* @package voting_controls_phpBB3
* @version $Id: vote.php 8028 2009-06-15 23:44 sayakb $
* @copyright (c) 2009 Sayak Banerjee
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

var xmlhttp,i;
var exup,exdn,shup,shdn;
var gact;

function vote(act,votes,tid,uid,uname,uv,dv,vs,vp,key,sid)
{
	gact = act;
	xmlhttp=GetXmlHttpObject();
	FadeOpacity('voteCount',100,0,150,20);
	var vcint = parseInt(votes);
	setTimeout("document.getElementById('voteCount').style.display='none'",150);
	setTimeout("document.getElementById('upVotes').style.display='none'",150);
	setTimeout("document.getElementById('downVotes').style.display='none'",150);
	setTimeout("document.getElementById('spinner').style.display='block'",150);
	setTimeout("FadeOpacity('spinner',0,100,200,20)",150);

	if(act=='up')
	{
		uvtp=1;
	}
	else if(act=='dn')
	{
		uvtp=-1;
	}
	else
	{
		uvtp=0;
	}

	if(vcint==1) postfix=" "+vs;
	else postfix=" "+vp;

	votes=vcint+postfix;
	setTimeout("document.getElementById('voteCount').innerHTML='"+votes+"'",150);
	setCookie('key',key,2000);
	var url="viewtopic.php?t="+tid+"&action=vote&usrvote="+uvtp+"&uid="+uid+"&username="+uname+"&key="+key+"&sid="+sid;
	xmlhttp.onreadystatechange = wrapUpVote;
	xmlhttp.open("GET",url,true);
	xmlhttp.send(null);

	exup=parseInt(uv)+1;
	exdn=parseInt(dv)-1;
	shup='+'+exup;

	if(exdn>=0) shdn='-'+exdn;
	else shdn=exdn;

	if(act=='up') setTimeout("document.getElementById('upVotes').innerHTML='"+shup+"'",650);
	else if(act=='dn') setTimeout("document.getElementById('downVotes').innerHTML='"+shdn+"'",650);
}

function wrapUpVote()
{
	if ( xmlhttp.responseText && document.getElementById('spinner').style.display == 'block' )
	{
		FadeOpacity('spinner',100,0,200,20);
		document.getElementById('upEnabled').style.display='none';
		document.getElementById('nlEnabled').style.display='none';
		document.getElementById('dnEnabled').style.display='none';
		if(gact=='up') document.getElementById('upDisabledz').style.display='inline';
		else document.getElementById('upDisabled').style.display='inline';
		if(gact=='dn') document.getElementById('dnDisabledz').style.display='inline';
		else document.getElementById('dnDisabled').style.display='inline';
		if(gact=='nl') document.getElementById('nlDisabledz').style.display='inline';
		else document.getElementById('nlDisabled').style.display='inline';
		document.getElementById('spinner').style.display='none';
		document.getElementById('voteCount').style.display='block';
		document.getElementById('upVotes').style.display='inline';
		document.getElementById('downVotes').style.display='inline';
		FadeOpacity('voteCount',0,100,100,20);
	}
}

function GetXmlHttpObject()
{
	if (window.XMLHttpRequest) return new XMLHttpRequest();
	if (window.ActiveXObject) return new ActiveXObject("Microsoft.XMLHTTP");
	return null;
}

function FadeOpacity(elemId, fromOpacity, toOpacity, time, fps)
{
	var steps = Math.ceil(fps * (time / 1000));
	var delta = (toOpacity - fromOpacity) / steps;

	FadeOpacityStep(elemId, 0, steps, fromOpacity, delta, (time / steps));
	return true;
}
function FadeOpacityStep(elemId, stepNum, steps, fromOpacity, delta, timePerStep)
{
	SetOpacity(document.getElementById(elemId), Math.round(parseInt(fromOpacity) + (delta * stepNum)));

	if (stepNum < steps)
	{
		setTimeout("FadeOpacityStep('" + elemId + "', " + (stepNum+1)
			+ ", " + steps + ", " + fromOpacity + ", "
			+ delta + ", " + timePerStep + ");",
			timePerStep);
	}
}
function SetOpacity(elem, opacityAsInt)
{
	var opacityAsDecimal = opacityAsInt;

	if (opacityAsInt > 100) opacityAsInt = opacityAsDecimal = 100;
	else if (opacityAsInt < 0) opacityAsInt = opacityAsDecimal = 0;

	opacityAsDecimal /= 100;
	if (opacityAsInt < 1)
	opacityAsInt = 1;

	elem.style.opacity = (opacityAsDecimal);
	elem.style.filter  = "alpha(opacity=" + opacityAsInt + ")";
}
function setCookie(c_name,value,expiredays)
{
	var exdate = new Date();
	exdate.setDate (exdate.getDate() + expiredays);
	document.cookie = c_name + "=" + escape(value) +
	((expiredays==null) ? "" : ";expires=" + exdate.toGMTString());
}

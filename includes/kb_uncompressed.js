/**
*
* @package kde_brainstorm_phpBB3
* @version $Id: kb.js sayakb $
* @copyright (c) 2010 Sayak Banerjee
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

var xmlhttp, xmlhttpv, request_type, sidinurl, peek, page, sortkey, filterkey;
var initial_size, page_scroll, rstatus, fico1, fico2;
var g_id, g_cid, g_dmode, g_oldid; // Store the topic_id and user_vote globally
var rpage, rfid;
var navpage = 1;
var sid = new Array();
var uid, mod_id, tool, chvt, hl1, hl2, stats;
var stb = 0;
var autor = 0;
var oit, nit, jtp = 0;
var commpage = 1;
var g_cat = document.location.hash;
var listen;

var clientPC = navigator.userAgent.toLowerCase(); // Get client info
var clientVer = parseInt(navigator.appVersion); // Get browser version

var is_ie = ((clientPC.indexOf('msie') != -1) && (clientPC.indexOf('opera') == -1)); // Browser is IE ?
var is_win = ((clientPC.indexOf('win') != -1) || (clientPC.indexOf('16bit') != -1)); // Windows OS ?

// Setup hash listener
function startListener()
{
	g_cat = document.location.hash;
	
	listen = setInterval(function() {
		if (document.location.hash != g_cat)
		{
			g_cat = document.location.hash;
			hashval = g_cat.replace('#', '');
			processHash(hashval, true);
		}
	}, 10);
}

function stopListener()
{
	clearInterval(listen);
}

// The onLoad function
function showMain(cat)
{
	var hashval = document.location.hash;
	sortkey = '';
	filterkey = '';
	showLoader();

	// Show Intro
	if (getIntVal(document.getElementById('show_intro').innerHTML))
	{
		showIntro();
	}

	// Find and set sid
	uid = getIntVal(document.getElementById('kb_uid').innerHTML);
	sid[uid] = getArg('sid');

	if (sid[uid] == 0)
	{
		sid[uid] = document.getElementById('kb_sid').innerHTML;
	}

	// Save the initial page size
	var arrayPageSize = new Array();
	arrayPageSize = getPageSize();
	initial_size = arrayPageSize[1];

	// Make the hash argument readable
	hashval = hashval.replace('#', '');

	// Make the main div visible
	document.getElementById('main').style.display = 'block';

	// Check if someone posted a non-js URL
	mode = getArg ('mode');
	idea = getArg ('i');
	peek = getArg ('c');
	page = getArg ('page');

	if (getArg ('f'))
	{
		cat = getArg ('f');
	}

	if (!mode && hashval && hashval.substr(0, 3) != 'cat' && hashval.substr(0, 4) != 'idea' && hashval.substr(0, 4) != 'post' && hashval != 'help' && hashval != 'tour' && hashval != 'reports' && hashval != 'vault' && hashval != 'anchortop' && hashval != 'anchormain' && hashval != 'anchordash')
	{
		document.getElementById('main').innerHTML = '';
		document.getElementById('hash_invalid').style.display = "block";
		return false;
	}

	// SID has been provided in URL?
	if (getArg('sid'))
	{
		sidinurl = 1;
	}
	else
	{
		sidinurl = 0;
	}

	// Redirect them to a # argument
	if ((mode == 'main' || !mode) && hashval != 'vault' && hashval.substr(0, 4) != 'idea')
	{
		document.location = 'brainstorm.php' + ((sidinurl) ? '?sid=' + sid[uid] : '') + '#cat' + cat + ((page > 1) ? '_page' + page : '');
		reloadIconToggle(1);
	}
	if (mode == 'idea')
	{
		var scto;

		if (hashval.substr(0, 6) == 'anchor')
		{
			scto = getIntVal(hashval.replace('anchor', ''));
		}
		
		document.location = 'brainstorm.php' + ((sidinurl) ? '?sid=' + sid[uid] : '') + '#idea' + idea + ((page && !peek) ? '_page' + page : '') + ((peek) ? '_comment' + peek : '') + ((scto) ? '_jumpto' + scto : '');
		reloadIconToggle(1);
	}
	if (mode == 'post')
	{
		document.location = 'brainstorm.php' + ((sidinurl) ? '?sid=' + sid[uid] : '') + '#post';
		reloadIconToggle(0);
	}
	if (mode == 'help')
	{
		document.location = 'brainstorm.php' + ((sidinurl) ? '?sid=' + sid[uid] : '') + '#help';
		reloadIconToggle(0);
	}
	if (mode == 'tour')
	{
		document.location = 'brainstorm.php' + ((sidinurl) ? '?sid=' + sid[uid] : '') + '#tour';
		reloadIconToggle(0);
	}
	if (mode == 'reports')
	{
		document.location = 'brainstorm.php' + ((sidinurl) ? '?sid=' + sid[uid] : '') + '#reports';
		reloadIconToggle(0);
	}
	if (mode == 'vault')
	{
		document.location = 'brainstorm.php' + ((sidinurl) ? '?sid=' + sid[uid] : '') + '#vault';
		reloadIconToggle(1);
	}

	// Hide the error notice
	document.getElementById('hash_invalid').style.display = "none";

	processHash(hashval);
	return false;
}

// Function to check the hash
function processHash(hashval, override)
{
	// Get the # arg strings
	if (hashval.substr(0, 3) == 'cat') // A category view?
	{
		if (hashval.indexOf("_") != -1) // Comment permalink provided ?
		{
			mult = hashval.split("_");
			hashval = getIntVal(mult[0].replace('cat', ''));
			page = getIntVal(mult[1].replace('page', ''));
		}
		else
		{
			peek = 0;
			hashval = getIntVal(hashval.replace('cat', ''));
		}
		
		if (typeof(override) != "undefined")
		{
			switchCategory(page, hashval, override);
		}
		else
		{
			switchCategory(page, hashval);
		}
	}
	else if (hashval.substr(0, 4) == 'idea') // Idea view ?
	{
		if (hashval.indexOf("_") != -1) // Comment permalink to jumpto ID provided ?
		{
			mult = hashval.split("_");
			hashval = getIntVal(mult[0].replace('idea', ''));

			if (mult[1].substr(0, 4) == 'page')
			{
				commpage = getIntVal(mult[1].replace('page', ''));
			}
			
			if (mult[1].substr(0, 7) == 'comment')
			{
				peek = getIntVal(mult[1].replace('comment', ''));
			}
			
			if (mult[1].substr(0, 6) == 'jumpto')
			{
				jtp = getIntVal(mult[1].replace('jumpto', ''));
			}
			
			if (mult.length == 3 && mult[2].substr(0, 6) == 'jumpto')
			{
				jtp = getIntVal(mult[2].replace('jumpto', ''));
			}
		}
		else
		{
			peek = 0;
			hashval = getIntVal(hashval.replace('idea', ''));
		}
		
		showIdea(peek, hashval);
	}
	else if (hashval.substr(0, 4) == 'post') // New post page ?
	{
		submitIdea();
	}
	else if (hashval.substr(0, 4) == 'help') // Help page ?
	{
		showHelpTour('help');
	}
	else if (hashval.substr(0, 4) == 'tour') // Tour page ?
	{
		showHelpTour('tour');
	}
	else if (hashval == 'reports') // Reports page ?
	{
		showReports();
	}
	else if (hashval == 'vault') // Idea Vault page ?
	{
		var vfid = getIntVal(document.getElementById('kb_sandboxfid').innerHTML);

		populateBasic (1, vfid);
		document.getElementById('kb_selcat').innerHTML = vfid;
	}
	else
	{
		selcat = document.getElementById('kb_selcat').innerHTML;
		selcat = selcat.replace('kb_nav_', '');
		hashval = selcat;
	}
}

// Pagination action
function switchPage(pg, cat)
{
	if (pg == page)
	{
		return false;
	}
	else
	{
		page = pg;

		if (document.location.hash != '#vault')
		{
			stopListener();
			document.location.hash = 'cat' + cat + ((page > 1) ? '_page' + page : '');
			startListener();
		}

		populateBasic(page, cat);
		return false;
	}
}

// Page jump function
function jumpToPage(comm)
{
	var pg = prompt('Enter page number:');
	
	if (comm == 1)
	{
		var maxc = getIntVal(document.getElementById('maxpages').innerHTML);
	}
	else
	{
		var cat = document.getElementById('kb_selcat').innerHTML;
		cat = getIntVal (cat.replace ('kb_nav_', ''));
		var max = getIntVal(document.getElementById('kb_max_pages').innerHTML);
	}

	if (pg !== null && !isNaN(pg) && pg > 0)
	{
		if (comm == 1 && pg <= maxc)
		{
			commpage = pg;
			showIdea (0, rfid);
		}
		else if (pg <= max)
		{
			switchPage(pg, cat);
		}
	}

	return false;
}

// Execute this when you click on the tabs
function switchCategory(pg, fid, override)
{
	page = pg;
	sortkey = '';
	filterkey = '';
	var oldid = document.getElementById('kb_selcat').innerHTML;
	var id = 'kb_nav_' + fid;
	request_type = 'main';

	if (oldid == id && typeof(override) == "undefined")
	{
		if (document.getElementById('kb_selcat_init').innerHTML == "1")
		{
			document.getElementById('kb_selcat_init').innerHTML = "0";
		}
		else
		{
			return false;
		}
	}

	if (document.location.hash != '#vault')
	{
		stopListener();
		document.location.hash = 'cat' + fid + ((page > 1) ? '_page' + page : '');
		startListener();
		
		selectTab(oldid, id);
	}

	showLoader();
	populateBasic(pg, fid);

	return false;
}

// Shows the main list of ideas
function populateBasic(pg, fid)
{
	reloadIconToggle(1);
	var val_fid = getIntVal(document.getElementById('kb_validfid').innerHTML);

	xmlhttp = GetXmlHttpObject();
	request_type = 'main';
	var url = "brainstorm.php?ajax=1";
	url = url + "&mode=main";
	url = url + "&f=" + ((sortkey) ? val_fid : fid);
	url = url + ((sortkey) ? "&sort=" + sortkey : "");
	url = url + ((filterkey) ? "&timefilter=" + filterkey : "");
	url = url + "&sid=" + sid[uid];

	if (sortkey || document.location.hash == '#vault')
	{
		var oldid = document.getElementById('kb_selcat').innerHTML;
		document.getElementById('kb_selcat').innerHTML = 'kb_nav_0';
		if (oldid != 'kb_nav_0')
		{
			selectTab (oldid, 'kb_nav_0');
		}
	}

	if (pg != 0)
	{
		url = url + "&page=" + pg;
	}

	if (!autor) // We dont need loader for auto reload
	{
		scrollToTop();
	}

	xmlhttp.onreadystatechange = populateAllpurposeResponse;
	xmlhttp.open("GET", url, true);
	xmlhttp.send(null);

	rpage = pg;
	rfid = fid;

	//setTimeout("autoReload()", 20000);
}

// Automatic reload for populateBasic
function autoReload()
{
	var hash = document.location.hash.replace('#', '');
	if (request_type == 'main' && xmlhttp.readyState == 4 &&  hash != 'popular' && hash != 'random' && hash != 'unvoted')
	{
		autor = 1;
		populateBasic(rpage, rfid);
	}
}

// Show reports
function showReports()
{
	xmlhttp = GetXmlHttpObject();
	request_type = 'shre';
	var url = "brainstorm.php?ajax=1";
	url = url + "&mode=reports";
	url = url + "&sid=" + sid[uid];

	ismod = getIntVal(document.getElementById('kb_bravo').innerHTML);

	if (!ismod)
	{
		return false;
	}
	else if (ismod)
	{
		showLoader();
		var oldid = document.getElementById('kb_selcat').innerHTML;
		document.getElementById('kb_selcat').innerHTML = 'kb_nav_0';
		if (oldid != 'kb_nav_0')
		{
			selectTab (oldid, 'kb_nav_0');
		}
	
		stopListener();
		document.location.hash = 'reports';
		startListener();
		
		reloadIconToggle(0);
		xmlhttp.onreadystatechange = showReportsResponse;
		xmlhttp.open("GET", url, true);
		xmlhttp.send(null);
	}
	return false;
}

function showReportsResponse()
{
	if (xmlhttp.responseText)
	{
		document.getElementById('main').innerHTML = '';
		document.getElementById('main').innerHTML = xmlhttp.responseText;
	}

	if (xmlhttp.readyState == 4)
	{
		setPageTitle();
		hideLoader();
	}
}

function manageReport(origid, dupeid, act)
{
	var url = "brainstorm.php?ajax=1";
	url = url + "&mode=reports";
	url = url + "&modaction=" + ((act == '1') ? "approve" : "delete");
	url = url + "&i=" + dupeid;
	url = url + "&origid=" + origid;
	url = url + "&sid=" + sid[uid];

	ismod = getIntVal(document.getElementById('kb_bravo').innerHTML);

	if (!ismod)
	{
		return false;
	}

	if (ismod)
	{
		showLoader();
		xmlhttp.onreadystatechange = manageReportResponse;
		xmlhttp.open("GET", url, true);
		xmlhttp.send(null);
	}
	return false;
}

function manageReportResponse()
{
	if (xmlhttp.readyState == 4)
	{
		hideLoader();
		showReports();
	}
}

// Show help
function showHelpTour(dest)
{
	reloadIconToggle(0);
	xmlhttp = GetXmlHttpObject();
	request_type = dest;
	var url = "brainstorm.php?ajax=1";
	url = url + "&mode=" + dest;

	if (dest == 'tour')
	{
		navpage = 1;
	}

	stopListener();
	document.location.hash = dest;
	startListener();
	
	showLoader();
	var oldid = document.getElementById('kb_selcat').innerHTML;
	document.getElementById('kb_selcat').innerHTML = 'kb_nav_0';
	if (oldid != 'kb_nav_0')
	{
		selectTab (oldid, 'kb_nav_0');
	}

	scrollToTop();
	xmlhttp.onreadystatechange = populateAllpurposeResponse;
	xmlhttp.open("GET", url, true);
	xmlhttp.send(null);

	return false;
}

// Tour navigation
function navTour(dir)
{
	var oldnavpage = navpage;

	if (dir == 'next')
	{
		navpage = navpage + 1;
	}
	else
	{
		navpage = navpage - 1;
	}

	if (navpage != 1)
	{
		document.getElementById('tour_prev_e').style.display = 'inline';
		document.getElementById('tour_prev_d').style.display = 'none';
	}
	else
	{
		document.getElementById('tour_prev_e').style.display = 'none';
		document.getElementById('tour_prev_d').style.display = 'inline';
	}

	if (navpage != 4)
	{
		document.getElementById('tour_next_e').style.display = 'inline';
		document.getElementById('tour_next_d').style.display = 'none';
	}
	else
	{
		document.getElementById('tour_next_e').style.display = 'none';
		document.getElementById('tour_next_d').style.display = 'inline';
	}

	fadeIcons(0, 1, 'tour_img' + oldnavpage, 'tour_img' + navpage);

	return false;
}

// Show individual idea or comment
function showIdea(pk, id)
{
	peek = pk;
	sortkey = '';
	filterkey = '';

	stopListener();
	var hval = 'idea' + id + ((commpage && !peek) ? '_page' + commpage : '') + ((peek) ? '_comment' + peek : '') + ((jtp) ? '_jumpto' + jtp : '');
	document.location.hash = hval;
	startListener();
	
	request_type = 'idea';

	var oldid = document.getElementById('kb_selcat').innerHTML;
	document.getElementById('kb_selcat').innerHTML = 'kb_nav_0';
	if (oldid != 'kb_nav_0')
	{
		selectTab (oldid, 'kb_nav_0');
	}
	g_oldid = oldid.replace('kb_nav_', ''); // Let's preserve the previously selected category ID

	showIdeaAlias (id);

	return false;
}

function showIdeaAlias(id)
{
	if (!peek)
	{
		reloadIconToggle(1);
	}
	else
	{
		reloadIconToggle(0);
	}

	xmlhttp = GetXmlHttpObject();
	request_type = 'idea';
	var url = "brainstorm.php?ajax=1";
	url = url + "&mode=idea";
	url = url + "&i=" + id;
	url = url + ((!peek) ? "&page=" + commpage : "");
	url = url + ((peek) ? "&c=" + peek : "");
	url = url + "&sid=" + sid[uid];
	peek = 0;

	if (!stb && !jtp)
	scrollToTop();
	xmlhttp.onreadystatechange = populateAllpurposeResponse;
	xmlhttp.open("GET", url, true);
	xmlhttp.send(null);

	rfid = id;
}

function showComment(id, cid)
{
	peek = cid;
	showIdea(cid, id);
	return false;
}

// Idea navigation
function ideaNav(act, id)
{
	var max = document.getElementById('maxpages').innerHTML;
	
	if (act == 'prev' && commpage > 1)
	{
		commpage = commpage - 1;
	}
	else if (act == 'next' && commpage < max)
	{
		commpage = commpage + 1;
	}
	else if (act == 'first')
	{
		commpage = 1;
	}
	else if (act == 'last')
	{
		commpage = max;
	}
	else if (act.substr(0, 4) == 'page')
	{
		act = getIntVal(act.replace('page', ''));
		commpage = act;
	}
	
	showIdea(0, id);
	
	return false;
}

// Jump to last unread post
function jumpToUnread(id)
{
	jumpToUnreadAlias(id);
	return false;
}

function jumpToUnreadAlias(id)
{
	xmlhttp = GetXmlHttpObject();
	request_type = 'unrd';
	var url = "brainstorm.php?ajax=1";
	url = url + "&mode=idea";
	url = url + "&i=" + id;
	url = url + "&a=unread";
	url = url + "&sid=" + sid[uid];

	if (!stb && !jtp)
	scrollToTop();
	xmlhttp.onreadystatechange = jumpToUnreadResponse;
	xmlhttp.open("GET", url, true);
	xmlhttp.send(null);

	rfid = id;
}

function jumpToUnreadResponse()
{
	if (xmlhttp.responseText && xmlhttp.readyState == 4)
	{
		var respArr = new Array();
		var resp = xmlhttp.responseText;
		
		respArr = resp.split('+');
		
		commpage = respArr[0];
		jtp = respArr[1];	
			
		showIdea(0, rfid);
	}
}

// Submit new idea
function submitIdea()
{
	reloadIconToggle(0);
	
	stopListener();
	document.location.hash = 'post';
	startListener();

	var oldid = document.getElementById('kb_selcat').innerHTML;
	document.getElementById('kb_selcat').innerHTML = 'kb_nav_0';
	selectTab (oldid, 'kb_nav_0');

	showLoader();
	submitIdeaAlias();

	return false;
}

function submitIdeaAlias(id)
{
	xmlhttp = GetXmlHttpObject();
	request_type = 'subi';
	var url = "brainstorm.php?ajax=1";
	url = url + "&mode=post";
	url = url + "&sid=" + sid[uid];

	xmlhttp.onreadystatechange = populateAllpurposeResponse;
	xmlhttp.open("GET", url, true);
	xmlhttp.send(null);
}

function previewIdeaPost()
{
	handleIdeaPostAlias();
	return false;
}

function handleIdeaPostAlias()
{
	reloadIconToggle(0);
	var title = document.getElementById('ideatitle').value;
	var message = document.getElementById('newidea').value;
	var tags = document.getElementById('ideatags').value;

	title = Url.encode (title);
	message = message.replace(/\+/g, '@kb_sc');
	message = message.replace(/\%/g, '@kb_pc');
	message = Url.encode (message);
	tags = Url.encode (tags);

	xmlhttp = GetXmlHttpObject();
	request_type = 'prev';

	if (message.length > 6400)
	{
		alert('Text too long. Please reduce the size of the text, or use the non-AJAX form for this idea/comment.');
		return false;
	}
	else
	{
		var url = "brainstorm.php?ajax=1";
		url = url + "&mode=post";
		url = url + "&a=postmsg";
		url = url + "&title=" + title;
		url = url + "&msg=" + message;
		url = url + "&tags=" + tags;
		url = url + "&preview=1";
		url = url + "&sid=" + sid[uid];
	}

	if (message.length < 10)
	{
		alert('Message too short');
		return false;
	}
	else if (title.length < 5)
	{
		alert('Idea title too short');
		return false;
	}
	else
	{
		showLoader();
		xmlhttp.onreadystatechange = populateAllpurposeResponse;
		xmlhttp.open("GET", url, true);
		xmlhttp.send(null);
	}
}

// For filling in the main div with AJAX returned text
function populateAllpurposeResponse()
{
	var maindiv = document.getElementById('main');

	if (xmlhttp.responseText && xmlhttp.responseText != 'done' && !autor && request_type != 'vote')
	{
		maindiv.innerHTML = xmlhttp.responseText;
	}

	if (xmlhttp.responseText && xmlhttp.responseText != 'done' && (autor || request_type == 'tour') && xmlhttp.readyState == 4 && request_type != 'vote')
	{
		maindiv.innerHTML = xmlhttp.responseText;
	}

	if (xmlhttp.readyState == 4)
	{
		setPageTitle();

		hideLoader();
		if (request_type == 'idea')
		{
			document.getElementById('addcomm').style.display = 'block';
			document.getElementById('newcomm').style.display = 'none';

			if (stb)
			{
				stb = 0;
				smoothScroll('kb_bottom');
			}
			
			if (jtp)
			{
				var tjtp = jtp;
				var hval = 'idea' + rfid + ((commpage && !peek) ? '_page' + commpage : '') + ((peek) ? '_comment' + peek : '');
				jtp = 0;
				window.location = '#anchor' + tjtp;
				
				stopListener();
				document.location.hash = hval;
				startListener();
				
				smoothScroll('anchor' + tjtp);
			}
		}
		
		if (request_type == 'main' || request_type == 'idea')
		{
			if (autor)
			{
				autor = 0;
			}
		}
	}
}

// Post new idea
function newIdea()
{
	sortkey = '';
	filterkey = '';
	reloadIconToggle(0);
	xmlhttp = GetXmlHttpObject();
	request_type = 'post';
	var message = document.getElementById('newidea').value;
	var title = document.getElementById('ideatitle').value;
	var ideatags = document.getElementById('ideatags').value;

	if (!trim(message))
	{
		alert('Message too short');
		return false;
	}
	if (!trim(title))
	{
		alert('Idea title too short');
		return false;
	}

	title = Url.encode (title);
	message = message.replace(/\+/g, '@kb_sc');
	message = message.replace(/\%/g, '@kb_pc');
	message = Url.encode (message);
	ideatags = Url.encode (ideatags);

	if (message.length > 6400)
	{
		alert('Text too long. Please reduce the size of the text, or use the non-AJAX form for this idea/comment');
		return false;
	}
	else
	{
		var url = "brainstorm.php?ajax=1";
		url = url + "&mode=post";
		url = url + "&a=postmsg";
		url = url + "&title=" + title;
		url = url + "&msg=" + message;
		url = url + ((ideatags) ? "&tags=" + ideatags : "");
		url = url + "&submit=1";
		url = url + "&sid=" + sid[uid];
	}

	if (message.length < 10)
	{
		alert('Message too short');
		return false;
	}
	else if (title.length < 5)
	{
		alert('Idea title too short');
		return false;
	}
	else
	{
		showLoader();
		xmlhttp.onreadystatechange = newIdeaResponse;
		xmlhttp.open("GET", url, true);
		xmlhttp.send(null);
	}
	return false;
}

function newIdeaResponse()
{
	if (xmlhttp.responseText)
	{
		n_id = getIntVal(xmlhttp.responseText); // AJAX will return the new idea ID

		var message = document.getElementById('newidea').value = "";
		var title = document.getElementById('ideatitle').value = "";

		showIdea (0, n_id);
	}

	if (xmlhttp.readyState == 4)
	{
		hideLoader();
	}
}

// Report duplicate
function dupeReport(id, cid, act)
{
	showLoader();
	if (act == 1)
	{
		document.getElementById('reportform').style.display = 'block';
		document.getElementById('reportbr').innerHTML = '<br />';
		document.getElementById('origid').value = '';
	}
	else if (act == 2)
	{
		var origid = document.getElementById('origid').value;
		xmlhttp = GetXmlHttpObject();
		request_type = 'rept';
		var url = "brainstorm.php?ajax=1";
		url = url + "&mode=dupe";
		url = url + "&i=" + id;
		url = url + "&c=" + cid;
		url = url + "&origid=" + origid;
		url = url + "&sid=" + sid[uid];

		g_id = id;

		xmlhttp.onreadystatechange = dupeReportResponse;
		xmlhttp.open("GET", url, true);
		xmlhttp.send(null);
	}
	else if (act == 3)
	{
		document.getElementById('reportform').style.display = 'none';
		document.getElementById('reportbr').innerHTML = '';
	}
	else
	{
		return false;
	}

	hideLoader();
	return false;
}

function dupeReportResponse()
{
	if (xmlhttp.responseText)
	{
		if (xmlhttp.responseText == 'done')
		{
			document.getElementById('repsent').style.display = 'block';
			document.getElementById('reperr').style.display = 'none';
			document.getElementById('repnotbr').innerHTML = '<br />';
			document.getElementById('reportform').style.display = 'none';
			document.getElementById('reportbr').innerHTML = '';

			ismod = getIntVal(document.getElementById('kb_bravo').innerHTML);
			if (ismod)
			{
				document.getElementById('dupereports').style.display = 'block';
			}

			scrollToTop();
		}
		else if (xmlhttp.responseText == 'reperr')
		{
			document.getElementById('reperr').style.display = 'block';
			document.getElementById('repsent').style.display = 'none';
			document.getElementById('repnotbr').innerHTML = '<br />';
			scrollToTop();
		}
	}


	if (xmlhttp.readyState == 4)
	{
		hideLoader();
	}
}

// Idea moderation
function unmarkDupe(id)
{
	var vid = getIntVal(document.getElementById('kb_validfid').innerHTML);
	document.getElementById('modaction').value = 'moveto' + vid;
	modTools(id);
	return false;
}

function modTools(id)
{
	tool = document.getElementById('modaction').value;
	var did = getIntVal(document.getElementById('kb_dupefid').innerHTML);

	if (!tool)
	return false;

	if (tool != 'approve' && tool != 'unapprove' && tool != 'trend' && tool != 'stats' && tool != 'hstats' && tool != 'htrend' && tool != 'moveto' + did)
	{
		if (!confirm('Are you sure you want to perform this operation?'))
		{
			document.getElementById('modaction').value = "";
			return false;
		}
		else
		{
			document.getElementById('modaction').value = "";
		}
	}

	var orig = 0;
	if (tool.substr(0,6) == 'moveto' && getIntVal(tool.replace('moveto', '')) == did)
	{
		orig = getIntVal(prompt('Enter original idea #:'));
		if (!orig)
		{
			alert('Invalid idea specified');
			document.getElementById('modaction').value = "";
			return false;
		}
	}

	if (tool == 'htrend' || tool == 'hstats')
	{
		document.getElementById('votetrend').style.display = 'none';
		document.getElementById(tool.substr(1, 5)).disabled = false;
		document.getElementById(tool).disabled = true;
		document.getElementById('modaction').value = 'trend';
		document.getElementById('vtbrk').innerHTML = "";
	}

	ismod = getIntVal(document.getElementById('kb_bravo').innerHTML);
	sortkey = '';
	filterkey = '';
	xmlhttp = GetXmlHttpObject();
	request_type = 'imtl';
	mod_id = id;

	var url = "brainstorm.php?ajax=1";
	url = url + "&mode=mod";
	url = url + "&a=modtool";
	url = url + "&modaction=" + tool;
	url = url + ((orig) ? "&orig=" + orig : "");
	url = url + "&i=" + id;
	url = url + "&sid=" + sid[uid];

	showLoader();
	xmlhttp.onreadystatechange = modToolsResponse;
	xmlhttp.open("GET", url, true);
	xmlhttp.send(null);
	return false;
}

function modToolsResponse()
{
	if (xmlhttp.responseText)
	{
		if (xmlhttp.responseText == 'done' && tool != 'htrend' && tool != 'hstats')
		{
			document.getElementById('inverr').style.display = 'none';
			document.getElementById('trid').style.display = 'none';
			hideLoader();
			showIdea (0, mod_id);
		}
		else if (xmlhttp.responseText == 'dupeinv')
		{
			document.getElementById('inverr').style.display = 'block';
			document.getElementById('trid').style.display = 'block';
			hideLoader();
		}
		else
		{
			if (tool != 'htrend' && tool != 'hstats')
			{
				document.getElementById('votetrend').innerHTML = xmlhttp.responseText;
				document.getElementById('votetrend').style.display = "block";
				document.getElementById('vtbrk').innerHTML = "<br />";
			}

			if (tool == 'trend')
			{
				document.getElementById('trend').disabled = true;
				document.getElementById('htrend').disabled = false;
				document.getElementById('stats').disabled = false;
				document.getElementById('hstats').disabled = true;
				document.getElementById('modaction').value = 'htrend';
			}
			if (tool == 'stats')
			{
				document.getElementById('trend').disabled = false;
				document.getElementById('htrend').disabled = true;
				document.getElementById('stats').disabled = true;
				document.getElementById('hstats').disabled = false;
				document.getElementById('modaction').value = 'hstats';
			}
			hideLoader();

			rfid = mod_id;
		}
	}

	if (xmlhttp.readyState == 4)
	{
		hideLoader();
	}
}

function modControl(id, act, stat)
{
	ismod = getIntVal(document.getElementById('kb_bravo').innerHTML);
	sortkey = '';
	filterkey = '';
	xmlhttp = GetXmlHttpObject();
	request_type = 'imgr';
	mod_id = id;
	tool = act;
	stats = stat;

	var url = "brainstorm.php?ajax=1";
	url = url + "&mode=mod";
	url = url + "&a=" + act;
	url = url + "&i=" + id;
	url = url + "&sid=" + sid[uid];

	showLoader();
	xmlhttp.onreadystatechange = modControlResponse;
	xmlhttp.open("GET", url, true);
	xmlhttp.send(null);
	return false;
}

function modControlResponse()
{
	if (xmlhttp.responseText)
	{
		document.getElementById('arr_unapproved_' + mod_id).style.display = 'none';

		if (stats == -2 && tool == 'approve')
		{
			document.getElementById('arr_enabled_' + mod_id).style.display = 'inline';
		}
		else if (tool == 'approve')
		{
			if (stats == 1)
			{
				document.getElementById('arr_disabled_up_' + mod_id).style.display = 'inline';
				document.getElementById('arr_disabled_nlg_' + mod_id).style.display = 'inline';
				document.getElementById('arr_disabled_dng_' + mod_id).style.display = 'inline';
			}
			else if (stats == 0)
			{
				document.getElementById('arr_disabled_upg_' + mod_id).style.display = 'inline';
				document.getElementById('arr_disabled_nl_' + mod_id).style.display = 'inline';
				document.getElementById('arr_disabled_dng_' + mod_id).style.display = 'inline';
			}
			else if (stats == -1)
			{
				document.getElementById('arr_disabled_upg_' + mod_id).style.display = 'inline';
				document.getElementById('arr_disabled_nlg_' + mod_id).style.display = 'inline';
				document.getElementById('arr_disabled_dn_' + mod_id).style.display = 'inline';
			}
			else
			{
				document.getElementById('arr_enabled_' + mod_id).style.display = 'inline';
			}
		}
		else if (tool == 'reject')
		{
			document.getElementById('arr_locked_' + mod_id).style.display = 'inline';
		}
	}

	if (xmlhttp.readyState == 4)
	{
		hideLoader();

		if (tool == 'approve' || tool == 'reject')
		{
			if (tool == 'approve')
			{
				document.getElementById('ideastatus_' + mod_id + '_sandbox').style.display = 'none';
				document.getElementById('ideastatus_' + mod_id + '_valid').style.display = 'inline';
			}
			else
			{
				document.getElementById('ideastatus_' + mod_id + '_sandbox').style.display = 'none';
				document.getElementById('ideastatus_' + mod_id + '_invalid').style.display = 'inline';
			}

			if (document.location.hash != '#vault')
			{
				if (document.getElementById('modtoolua1').disabled === false)
				{
					document.getElementById('modtoolua1').disabled = true;
					document.getElementById('modtoolua2').disabled = false;
				}
				else
				{
					document.getElementById('modtoolua1').disabled = false;
					document.getElementById('modtoolua2').disabled = true;
				}
			}
		}
	}
}

// Voting function
function chVote(vote, id)
{
	chvt = 1;
	doVote(vote, id);
	return false;
}

function doVote(vote, id)
{
	xmlhttpv = GetXmlHttpObject();
	request_type = 'vote';
	var url = "brainstorm.php?ajax=1";
	url = url + "&mode=vote";
	url = url + "&v=" + vote;
	url = url + "&i=" + id;
	url = url + "&sid=" + sid[uid];

	votecount = getIntVal(document.getElementById('votecountclean_' + id).innerHTML);
	upvotes = getIntVal(document.getElementById('upvotesclean_' + id).innerHTML);
	downvotes = getIntVal(document.getElementById('downvotesclean_' + id).innerHTML);

	if (vote == 'up')
	{
		votecount = votecount + 1;
		upvotes = upvotes + 1;
	}
	else if (vote == 'dn')
	{
		votecount = votecount - 1;
		downvotes = downvotes + 1;
	}

	if (chvt)
	{
		var uea = document.getElementById('arr_disabled_up_' + id).style.display;
		var dea = document.getElementById('arr_disabled_dn_' + id).style.display;

		if (vote == 'up' && dea == 'inline')
		{
			votecount = votecount + 1;
			downvotes = downvotes - 1;
		}
		else if (vote == 'nl' && dea == 'inline')
		{
			votecount = votecount + 1;
			downvotes = downvotes - 1;
		}
		else if (vote == 'nl' && uea == 'inline')
		{
			votecount = votecount - 1;
			upvotes = upvotes - 1;
		}
		else if (vote == 'dn' && uea == 'inline')
		{
			votecount = votecount - 1;
			upvotes = upvotes - 1;
		}
	}

	var up_arr, nl_arr, dn_arr;
	up_arr = 'upg';
	nl_arr = 'nlg';
	dn_arr = 'dng';

	if (vote == 'up')
	{
		up_arr = 'up';
	}
	else if (vote == 'nl')
	{
		nl_arr = 'nl';
	}
	else if (vote == 'dn')
	{
		dn_arr = 'dn';
	}

	document.getElementById('arr_enabled_' + id).style.display = 'none';
	document.getElementById('arr_disabled_up_' + id).style.display = 'none';
	document.getElementById('arr_disabled_dn_' + id).style.display = 'none';
	document.getElementById('arr_disabled_nl_' + id).style.display = 'none';
	document.getElementById('arr_disabled_upg_' + id).style.display = 'none';
	document.getElementById('arr_disabled_dng_' + id).style.display = 'none';
	document.getElementById('arr_disabled_nlg_' + id).style.display = 'none';
	document.getElementById('arr_disabled_' + up_arr + '_' + id).style.display = 'inline';
	document.getElementById('arr_disabled_' + nl_arr + '_' + id).style.display = 'inline';
	document.getElementById('arr_disabled_' + dn_arr + '_' + id).style.display = 'inline';

	showLoader();

	document.getElementById('votecount_' + id).innerHTML = votecount;
	document.getElementById('upvotes_' + id).innerHTML = upvotes;
	document.getElementById('downvotes_' + id).innerHTML = downvotes;
	document.getElementById('votecountclean_' + id).innerHTML = votecount;
	document.getElementById('upvotesclean_' + id).innerHTML = upvotes;
	document.getElementById('downvotesclean_' + id).innerHTML = downvotes;

	xmlhttpv.onreadystatechange = doVoteResponse;
	xmlhttpv.open("GET", url, true);
	xmlhttpv.send(null);

	return false;
}

function doVoteResponse()
{
	if (xmlhttpv.readyState == 4 && xmlhttpv.responseText)
	{
		hideLoader();
	}
}

// Inline idea/comment editing
function commentEdit(cid, tin)
{
	message = document.getElementById('comment_clean' + cid).innerHTML;
	message = message.replace(/\&amp;/g, "&");
	message = message.replace(/\\\'/g, "'");
	message = message.replace(/\&lt;/g, "<");
	message = message.replace(/\&gt;/g, ">");
	message = message.replace(/\<br \/\>/g, "\n");

	document.getElementById('comment' + cid).style.display = 'none';
	document.getElementById('editor' + cid).style.display = 'block';

	document.getElementById('editbox' + cid).value = message;
	document.getElementById('editbox' + cid).focus();

	if (tin)
	{
		oit = document.getElementById('edittitle' + cid).value;
	}
	else
	{
		oit = '';
		nit = '';
	}

	return false;
}

function commentEditHide(cid, tin)
{
	document.getElementById('comment' + cid).style.display = 'block';
	document.getElementById('editor' + cid).style.display = 'none';
	if (tin) document.getElementById('edittitle' + cid).value = oit;

	return false;
}

function commentUpdate(id, cid, tin)
{
	xmlhttp = GetXmlHttpObject();
	request_type = 'comu';
	var message = document.getElementById('editbox' + cid).value;
	var title = "null";
	if (tin)
	{
		title = document.getElementById('edittitle' + cid).value;
		nit = title;
	}

	message = message.replace(/\+/g, '@kb_sc');
	message = message.replace(/\%/g, '@kb_pc');
	title = title.replace(/\+/g, '@kb_sc');
	title = title.replace(/\%/g, '@kb_pc');
	message = Url.encode (message);
	title = Url.encode (title);

	if (!trim(message))
	{
		document.getElementById('comment' + cid).style.display = 'block';
		document.getElementById('editor' + cid).style.display = 'none';
		return false;
	}

	if (message.length > 6400)
	{
		alert('Text too long. Please reduce the size of the text, or use the non-AJAX form for this idea/comment.');
		return false;
	}
	else
	{
		var url = "brainstorm.php?ajax=1";
		url = url + "&mode=idea";
		url = url + "&i=" + id;
		url = url + "&page=" + commpage;
		url = url + "&e=" + cid;
		url = url + "&a=updatemsg";
		url = url + (tin ? "&title=" + title : "");
		url = url + "&msg=" + message;
		url = url + "&sid=" + sid[uid];
	}

	g_cid = cid;

	if (message.length < 10)
	{
		alert('Message too short');
		document.getElementById('editbox' + cid).focus();
		return false;
	}
	else if (title.length < 5 && tin)
	{
		alert('Idea title too short');
		document.getElementById('edittitle' + cid).focus();
		return false;
	}
	else
	{
		showLoader();
		xmlhttp.onreadystatechange = commentUpdateResponse;
		xmlhttp.open("GET", url, true);
		xmlhttp.send(null);
	}
	return false;
}

function commentUpdateResponse()
{
	if (xmlhttp.responseText)
	{
		var tbox = document.getElementById('editbox' + g_cid).value;

		document.getElementById('comment' + g_cid).innerHTML = xmlhttp.responseText;
		document.getElementById('comment_clean' + g_cid).innerHTML = tbox;
		document.getElementById('comment' + g_cid).style.display = 'block';
		document.getElementById('editor' + g_cid).style.display = 'none';

		if (oit != nit || oit.length != nit.length)
		{
			document.getElementById('idea_title').innerHTML = '<h2>' + nit + '</h2>';
		}
	}

	if (xmlhttp.readyState == 4)
	{
		hideLoader();
	}
}

// Insert comment functions
function commentInsert(id)
{
	xmlhttp = GetXmlHttpObject();
	request_type = 'comi';
	var message = document.getElementById('commentbox').value;
	
	message = message.replace(/\+/g, '@kb_sc');
	message = message.replace(/\%/g, '@kb_pc');
	message = Url.encode (message);
	var url = "brainstorm.php?ajax=1";
	url = url + "&mode=idea";
	url = url + "&i=" + id;
	url = url + "&page=" + commpage;
	url = url + "&a=newmsg";
	url = url + "&nmsg=" + message;
	url = url + "&lp=" + document.getElementById('lastpost').innerHTML;
	url = url + "&cc=" + document.getElementById('commentcount').innerHTML;
	url = url + "&ilp=" + document.getElementById('islastpage').innerHTML;
	url = url + "&sid=" + sid[uid];

	g_id = id;

	if (message.length < 10)
	{
		alert('Message too short');
		document.getElementById('commentbox').value = "";
		document.getElementById('newcomm').style.display = 'none';
		hideLoader();
		return false;
	}
	else
	{
		showLoader();
		xmlhttp.onreadystatechange = commentInsDelResponse;
		xmlhttp.open("GET", url, true);
		xmlhttp.send(null);
	}
	return false;
}

function commentDelete (dmode, id, cid)
{
	xmlhttp = GetXmlHttpObject();
	request_type = 'comd';

	var url = "brainstorm.php?ajax=1";
	url = url + "&mode=idea";
	url = url + "&i=" + id;
	url = url + "&page=" + commpage;
	url = url + "&e=" + cid;
	url = url + "&a=delmsg";
	url = url + "&sid=" + sid[uid];

	g_id = id;
	g_dmode = dmode;

	var uchoice = confirm('Are you sure you want to delete this ' + ((dmode == 1) ? 'comment?' : 'idea?'));
	if (uchoice)
	{
		showLoader();
		xmlhttp.onreadystatechange = commentInsDelResponse;
		xmlhttp.open("GET", url, true);
		xmlhttp.send(null);
	}
	return false;
}

// Response for insert and delete functions
function commentInsDelResponse()
{
	if (xmlhttp.responseText)
	{
		document.getElementById('newcomm').style.display = 'none';
		document.getElementById('commentbox').value = "";
		if (request_type == 'comi')
		{
			if (xmlhttp.readyState == 4)
			{
				if (xmlhttp.responseText == 'reloadpage_newcomm')
				{
					stb = 1;
					showIdeaAlias (g_id);
				}
				else if (xmlhttp.responseText == 'reloadpage_wrongpg')
				{
					var sat = getIntVal(document.getElementById('saturated').innerHTML);
					var mpgs = getIntVal(document.getElementById('maxpages').innerHTML);

					if (sat == 1) // Add a new page
					{
						mpgs = mpgs + 1;
					}
					
					// Goto last page
					commpage = mpgs;
					
					stb = 1;
					showIdeaAlias (g_id);
				}
				else
				{
					var commentList = document.getElementById('newajaxcomm');
					commentList.innerHTML = commentList.innerHTML + xmlhttp.responseText;
					document.getElementById('commentcount').innerHTML = getIntVal(document.getElementById('commentcount').innerHTML) + 1;

					var lp = document.getElementById('lastpost').innerHTML;
					document.getElementById('lastpost').innerHTML = document.getElementById('newajaxcomm' + lp).innerHTML;
				}
			}
		}
		else
		{
			commpage = 1;
			
			if (g_dmode == 1)
			{
				showIdea (0, g_id);
			}
			else
			{
				switchCategory (1, getIntVal(document.getElementById('kb_validfid').innerHTML));
			}
		}
	}
}

// Idea tag functions
function tagEditor()
{
	var taglist = document.getElementById('taglist_clean').innerHTML;
	document.getElementById('tags').style.display = 'none';
	document.getElementById('tag_editor').style.display = 'block';

	document.getElementById('tag_textbox').value = taglist;
	document.getElementById('tag_textbox').focus();

	return false;
}

function tagEditorHide()
{
	document.getElementById('tags').style.display = 'block';
	document.getElementById('tag_editor').style.display = 'none';

	return false;
}

function tagUpdate(id)
{
	xmlhttp = GetXmlHttpObject();
	request_type = 'tags';
	var tags = document.getElementById('tag_textbox').value;

	tags = Url.encode (tags);

	var url = "brainstorm.php?ajax=1";
	url = url + "&mode=idea";
	url = url + "&i=" + id;
	url = url + "&page=" + commpage;
	url = url + "&a=updatetags";
	url = url + "&tags=" + tags;
	url = url + "&sid=" + sid[uid];

	showLoader();

	xmlhttp.onreadystatechange = tagUpdateResponse;
	xmlhttp.open("GET", url, true);
	xmlhttp.send(null);

	return false;
}

function tagUpdateResponse()
{
	if (xmlhttp.responseText)
	{
		document.getElementById('tags').innerHTML = xmlhttp.responseText;
		document.getElementById('tags').style.display = 'block';
		document.getElementById('tag_editor').style.display = 'none';
	}

	if (xmlhttp.readyState == 4)
	{
		hideLoader();
	}
}

// Hide intro text
function showIntro()
{
	document.getElementById('intro').style.opacity = '0';
	document.getElementById('intro').style.filter  = 'alpha(opacity=0)';
	document.getElementById('intro').style.display = 'block';
	fadeOpacity('intro', 0);
}

function hideIntro(href)
{
	fadeOpacity('intro', 1);
	
	setTimeout(function() {
		var xmlhttp = GetXmlHttpObject();
		xmlhttp.open("GET", href, false);
		xmlhttp.send(null);	
	}, 500);
}

// Toggle the dashboard
function toggleDashboard()
{
	var dash = document.getElementById('dashboard');

	if (dash.style.display == 'block')
	{
		fadeOpacity('dashboard', 1);
		setCookie('kb_dash', '0', 1000);
	}
	else
	{
		document.getElementById('dashtime').style.display = 'none';
		document.getElementById('dashicons').style.display = 'block';
		document.getElementById('dashicons').style.opacity = '1';
		setOpacity('dashboard', 0);
		fadeOpacity('dashboard', 0);
		setCookie('kb_dash', '1', 1000);
	}

	return false;
}

// Watch / unwatch topic
function watchIdea(id, act)
{
	xmlhttp = GetXmlHttpObject();
	request_type = 'subs';

	var url = "brainstorm.php?ajax=1";
	url = url + "&mode=idea";
	url = url + "&i=" + id;
	url = url + "&page=" + commpage;
	url = url + "&a=" + (act ? "watch" : "unwatch");
	url = url + "&sid=" + sid[uid];

	showLoader();

	xmlhttp.onreadystatechange = watchIdeaResponse;
	xmlhttp.open("GET", url, true);
	xmlhttp.send(null);

	return false;
}

function watchIdeaResponse()
{
	if (xmlhttp.responseText == 'done' && xmlhttp.readyState == 4)
	{
		var sub = document.getElementById('idea-sub');
		var unsub = document.getElementById('idea-unsub');

		if (sub.style.display == 'inline')
		{
			sub.style.display = 'none';
			unsub.style.display = 'inline';
		}
		else
		{
			sub.style.display = 'inline';
			unsub.style.display = 'none';
		}
		hideLoader();
	}
	if (xmlhttp.responseText != 'done' && xmlhttp.readyState == 4)
	{
		hideLoader();
	}
}

// Quote comment
function quoteComment(cid)
{
	var cbox = document.getElementById('newcomm');
	var comment = document.getElementById('comment_clean' + cid).innerHTML;
	var author = document.getElementById('comment_author' + cid).innerHTML;

	cbox.style.display = 'block';
	document.getElementById('commentbox').value += '[quote="' + author + '"]' + comment + '[/quote]';
	document.getElementById('commentbox').focus();
	smoothScroll('kb_bottom');
	return false;
}

// Toggle the new comment form
function toggleAddComm()
{
	var cbox = document.getElementById('newcomm');

	if (cbox.style.display == 'none')
	{
		cbox.style.display = 'block';
		document.getElementById('commentbox').value = '';
		document.getElementById('commentbox').focus();
	}
	else
	{
		cbox.style.display = 'none';
	}

	return false;
}

// Insert smiley on post page
function insertText (code)
{
	var message = document.getElementById('newidea');

	message.value = message.value + code;
	message.focus();
	return false;
}

// Insert BBCode
// Taken from phpBB 3.0.5 code and modified for use
function bbFontStyle(bbopen, bbclose)
{
	theSelection = false;

	var message = document.getElementById('newidea');

	message.focus();

	if ((clientVer >= 4) && is_ie && is_win)
	{
		// Get text selection
		theSelection = document.selection.createRange().text;

		if (theSelection)
		{
			// Add tags around selection
			document.selection.createRange().text = bbopen + theSelection + bbclose;
			message.focus();
			theSelection = '';
			return false;
		}
	}
	else if (message.selectionEnd && (message.selectionEnd - message.selectionStart > 0))
	{
		mozWrap(message, bbopen, bbclose);
		message.focus();
		theSelection = '';
		return false;
	}

	// Set cursor position
	var cur_pos = message.selectionStart;
	var new_pos = cur_pos + bbopen.length;

	// Open tag
	insertText(bbopen + bbclose);

	// Center the cursor when we don't have a selection
	// Gecko and proper browsers
	if (!isNaN(message.selectionStart))
	{
		message.selectionStart = new_pos;
		message.selectionEnd = new_pos;
	}
	// IE
	else if (document.selection)
	{
		var range = message.createTextRange();
		range.move("character", new_pos);
		range.select();
	}

	return false;
}

function bbFontSize()
{
	var sel = document.getElementById('size').value;

	if (sel == '1')
	{
		bbFontStyle('[size=50]', '[/size]');
	}
	if (sel == '2')
	{
		bbFontStyle('[size=85]', '[/size]');
	}
	if (sel == '3')
	{
		bbFontStyle('[size=150]', '[/size]');
	}
	if (sel == '4')
	{
		bbFontStyle('[size=200]', '[/size]');
	}
}

function mozWrap(txtarea, open, close)
{
	var selLength = txtarea.textLength;
	var selStart = txtarea.selectionStart;
	var selEnd = txtarea.selectionEnd;
	var scrollTop = txtarea.scrollTop;

	if (selEnd == 1 || selEnd == 2)
	{
		selEnd = selLength;
	}

	var s1 = (txtarea.value).substring(0,selStart);
	var s2 = (txtarea.value).substring(selStart, selEnd);
	var s3 = (txtarea.value).substring(selEnd, selLength);

	txtarea.value = s1 + open + s2 + close + s3;
	txtarea.selectionStart = selEnd + open.length + close.length;
	txtarea.selectionEnd = txtarea.selectionStart;
	txtarea.focus();
	txtarea.scrollTop = scrollTop;

	return;
}

// Focus dashboard item
function focusDash(item)
{
	var items = new Array('dash_popular', 'dash_rand', 'dash_unvoted', 'dash_vault', 'dash_time');
	var descs = new Array('dash_popular_exp', 'dash_rand_exp', 'dash_unvoted_exp', 'dash_vault_exp', 'dash_time_exp', 'dash_none_exp');

	for (var i = 0; i < 6; i++)
	{
		if (item + '_exp' == descs[i])
		{
			document.getElementById(descs[i]).style.display = "block";
		}
		else
		{
			document.getElementById(descs[i]).style.display = "none";
		}
	}
	for (var j = 0; j < 5; j++)
	{
		if (item != items[j])
		setOpacity(items[j], '0.45');
	}
	return;
}

function unfocusDash(item)
{
	var items = new Array('dash_popular', 'dash_rand', 'dash_unvoted', 'dash_vault', 'dash_time');
	var descs = new Array('dash_popular_exp', 'dash_rand_exp', 'dash_unvoted_exp', 'dash_vault_exp', 'dash_time_exp', 'dash_none_exp');

	for (var i = 0; i < 6; i++)
	{
		if (descs[i] == 'dash_none_exp')
		{
			document.getElementById(descs[i]).style.display = "block";
		}
		else
		{
			document.getElementById(descs[i]).style.display = "none";
		}
	}
	for (var j = 0; j < 5; j++)
	{
		setOpacity(items[j], '1');
	}
	return;
}

// Setup the AJAX request
function GetXmlHttpObject()
{
	if (window.XMLHttpRequest) return new XMLHttpRequest();
	if (window.ActiveXObject) return new ActiveXObject("Microsoft.XMLHTTP");
	return null;
}

// Well, you gotta have some eyecandy ;)
function selectTab(oldid, id)
{
	if (!oldid || !id)
	{
		oldid = 'kb_nav_0';
		id = 'kb_nav_' + getIntVal(document.getElementById('kb_validfid').innerHTML);
	}

	if (id == 'kb_nav_0')
	{
		document.getElementById(oldid).style.height = '37px';
	}
	else
	{
		document.getElementById(id).style.height = '35px';
	}

	document.getElementById(oldid).style.background = "transparent";
	document.getElementById(oldid).style.fontWeight = "normal";
	document.getElementById(oldid).style.border = "1px transparent";

	document.getElementById(id).style.background = "#d3e4f3";
	document.getElementById(id).style.fontWeight = "bold";
	document.getElementById(id).style.border = "1px solid #a5b7c6";
	document.getElementById('kb_selcat').innerHTML = id;
	return false;
}

// Dashboard options
function selectDash(item)
{
	vfid = getIntVal(document.getElementById('kb_sandboxfid').innerHTML);

	if (item == 'popular' || item == 'random' || item == 'unvoted')
	{
		stopListener();
		var val_fid = getIntVal(document.getElementById('kb_validfid').innerHTML);
		document.location.hash = item;
		startListener();
		
		showLoader();
		sortkey = item;
		populateBasic (1, rfid);
	}

	if (item == 'vault')
	{
		showLoader();
		
		stopListener();
		document.location.hash = 'vault';
		startListener();
		
		var oldid = document.getElementById('kb_selcat').innerHTML;
		document.getElementById('kb_selcat').innerHTML = 'kb_nav_0';
		if (oldid != 'kb_nav_0')
		{
			selectTab (oldid, 'kb_nav_0');
		}
		sortkey = '';
		filterkey = '';
		populateBasic (1, vfid);
	}

	if (item == 'time')
	{
		fadeIcons(0);
	}

	return false;
}

// Time filters
function filterTime(act)
{
	if (act == 1)
	{
		fadeIcons(1);
	}
	else
	{
		for (var i = 0; i <= 5; i++)
		{
			if (document.getElementById('timefilter' + i).checked == 1)
			{
				showLoader();
				filterkey = i;
				populateBasic (1, rfid);
			}
		}
	}

	return false;
}

// Loader functions
function scrollToTop()
{
	var arrayPageScroll = new Array();

	arrayPageScroll = getPageScroll();

	page_scroll = arrayPageScroll[1];
	hideLoader();

	if (page_scroll > ((initial_size / 2) + (initial_size / 4)) && request_type != 'vote')
	{
		smoothScroll('kb_top');
	}
	else
	{
		showLoader();
	}
}

function showLoader()
{
	var height = 40;
	var width = 100;
	var arrayPageSize = new Array();
	var arrayPageScroll = new Array();
	document.getElementById('hash_invalid').style.display = "none";

	clearTimeout(hl1);
	clearTimeout(hl2);

	// Destroy the element if already exists
	if (document.getElementById('loader'))
	{
		//return false;
	}

	arrayPageSize = getPageSize();
	arrayPageScroll = getPageScroll();

	var top = arrayPageScroll[1];
	var left = ((arrayPageSize[0] - 20 - width) / 2);

	owner = document.getElementsByTagName("body").item(0);

	var loader = document.createElement("div");

	loader.style.background = "#022951";
	loader.style.opacity = "0.8";
	loader.style.color = "#FFFFFF";
	loader.style.position = "fixed";
	loader.style.zIndex = 1000;
	loader.style.textAlign = "center";
	loader.style.verticalAlign = "middle";
	loader.style.fontSize = "13px";
	loader.style.lineHeight = "40px";
	loader.innerHTML = '<strong>Loading</strong>';
	loader.style.width = width + "px";
	loader.style.height = height + "px";
	loader.style.top = "0px";
	loader.style.left = left + "px";
	loader.id = "loader";
	owner.insertBefore(loader, owner.firstChild);
}

function hideLoader()
{
	if (xmlhttp.readyState == 4)
	{
		hideLoaderNow();
	}

	setTimeout("hideLoader()", 1000);
}

function hideLoaderNow()
{
	var loader = document.getElementById('loader');

	if (loader)
	{
		document.body.removeChild(loader);
	}
}

// Function to set opacity
function setOpacity(item, val)
{
	inval = val * 100;

	if (inval < 1)
	{
		inval = 1;
	}
	document.getElementById(item).style.filter  = "alpha(opacity=" + inval + ")";
	document.getElementById(item).style.opacity = val;

	return true;
}

function fadeIcons(act, gen, item_out, item_in)
{
	var delay = 10;
	var opacity_out = 1;
	var opacity_in = 0;
	if (!gen)
	{
		var item_out = ((act == 1) ? 'dashtime' : 'dashicons');
		var item_in = ((act == 1) ? 'dashicons' : 'dashtime');
	}
	else
	{
		clearTimeout(fico1);
		clearTimeout(fico2);
	}

	var intop_out, intop_in;

	for (var i = 0; i <= 100; i++)
	{
		intop_out = opacity_out * 100;
		if (intop_out < 1) intop_out = 1;

		intop_in = opacity_in * 100;
		if (intop_in < 1) intop_in = 1;

		if (i < 50)
		{
			fico1 = setTimeout("document.getElementById('" + item_out + "').style.opacity = '" + opacity_out + "'", delay);
			fico2 = setTimeout("document.getElementById('" + item_out + "').style.filter = 'alpha(opacity = " + intop_out + ")'", delay);
			opacity_out -= 0.02;
		}
		else
		{
			if (i == 50)
			{
				setTimeout("document.getElementById('" + item_out + "').style.display = 'none'", delay);
				setTimeout("document.getElementById('" + item_in + "').style.display = 'block'", delay);
			}

			fico1 = setTimeout("document.getElementById('" + item_in + "').style.opacity = '" + opacity_in + "'", delay);
			fico2 = setTimeout("document.getElementById('" + item_in + "').style.filter = 'alpha(opacity = " + intop_in + ")'", delay);
			opacity_in += 0.02;
		}
		delay += 8;
	}

	hideLoader();
	return true;
}

function fadeOpacity(item, ftype)
{
	var delay = 10;
	var opacity = ((ftype == 1) ? 1 : 0);
	var vert = ((ftype == 1) ? 150 : 0);
	var intop, cvert, sndelay;

	for (var i = 0; i <= 350; i++)
	{
		intop = opacity * 100;
		if (intop < 1) intop = 1;

		if (i < 150 && !ftype)
		{
			cvert = vert + 'px';

			if (item == 'dashboard')
			{
				setTimeout("document.getElementById('slider').style.height = '" + cvert + "'", delay);
				delay += 1;
			}
			else
			{
				delay += 10;
			}

			sndelay = delay;
			vert += 1;

		}
		else if (i >= 150 && i < 200)
		{
			setTimeout("document.getElementById('slider').style.height = '0px'", sndelay);
			setTimeout("document.getElementById('" + item + "').style.opacity = '" + opacity + "'", delay);
			setTimeout("document.getElementById('" + item + "').style.filter = 'alpha(opacity = " + intop + ")'", delay);

			if (ftype == 1)
			{
				opacity -= 0.02;
			}
			else
			{
				setTimeout("document.getElementById('" + item + "').style.display = 'block'", delay);
				opacity += 0.02;
			}
			delay += 8;
		}
		else if (i >= 200 && ftype == 1)
		{
			if (i == 200)
			{
				setTimeout("document.getElementById('" + item + "').style.display = 'none'", delay);
			}
			cvert = vert + 'px';

			if (item == 'dashboard')
			setTimeout("document.getElementById('slider').style.height = '" + cvert + "'", delay);

			sndelay = delay;
			vert -= 1;
			delay += 1;
		}

	}
	return true;
}

// Function to reload page
function reloadPage()
{
	showLoader();
	hmode = document.location.hash;
	hmode = hmode.replace('#', '');

	if (hmode.substr(0, 3) == 'cat')
	{
		populateBasic (rpage, rfid);
	}
	else if (hmode.substr(0, 4) == 'idea')
	{
		showIdea (0, rfid);
	}
	else
	{
		hideLoader();
	}

	return false;
}

function reloadIconToggle(tag)
{
	if (uid == 1)
	{
		return false;
	}
	else
	{
		var enable = document.getElementById('reloadpage');

		if (tag == 1)
		{
			rstatus = 1;
			enable.style.display = 'inline';
		}
		else
		{
			rstatus = 0;
			enable.style.display = 'none';
		}
	}
}

// Set the page title
function setPageTitle()
{
	document.title = document.getElementById('pagetitle').innerHTML + ' • KDE Community Forums';
}

// Functions from quirksmode.org
// Modified for use by KDE Community Forums
function getPageScroll()
{
	var yScroll;

	if (self.pageYOffset)
	{
		yScroll = self.pageYOffset;
	}
	else if (document.documentElement && document.documentElement.scrollTop) // Explorer 6 Strict
	{
		yScroll = document.documentElement.scrollTop;
	}
	else if (document.body) // all other Explorers
	{
		yScroll = document.body.scrollTop;
	}
	arrayPageScroll = new Array('',yScroll);

	return arrayPageScroll;
}

function getPageSize()
{
	var xScroll, yScroll;

	if (window.innerHeight && window.scrollMaxY)
	{
		xScroll = document.body.scrollWidth;
		yScroll = window.innerHeight + window.scrollMaxY;
	}
	else if (document.body.scrollHeight > document.body.offsetHeight) // All but Explorer Mac
	{
		xScroll = document.body.scrollWidth;
		yScroll = document.body.scrollHeight;
	}
	else  // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
	{
		xScroll = document.body.offsetWidth;
		yScroll = document.body.offsetHeight;
	}

	var windowWidth, windowHeight;
	if (self.innerHeight) // all except Explorer
	{
		windowWidth = self.innerWidth;
		windowHeight = self.innerHeight;
	}
	else if (document.documentElement && document.documentElement.clientHeight)  // Explorer 6 Strict Mode
	{
		windowWidth = document.documentElement.clientWidth;
		windowHeight = document.documentElement.clientHeight;
	}
	else if (document.body) // other Explorers
	{
		windowWidth = document.body.clientWidth;
		windowHeight = document.body.clientHeight;
	}

	var pageHeight, pageWidth;

	// For small pages with total height less then height of the viewport
	if (yScroll < windowHeight)
	{
		pageHeight = windowHeight;
	}
	else
	{
		pageHeight = yScroll;
	}

	// For small pages with total width less then width of the viewport
	if (xScroll < windowWidth)
	{
		pageWidth = windowWidth;
	}
	else
	{
		pageWidth = xScroll;
	}

	var arrayPageSize = new Array(pageWidth,pageHeight,windowWidth,windowHeight);

	return arrayPageSize;
}

// A smooth scrolling function
function smoothScroll(id, flag)
{
	var startY = currentYPosition();
	var stopY = elmYPosition(id);
	var distance = stopY > startY ? stopY - startY : startY - stopY;
	var arrayPageSize = new Array();

	hideLoaderNow();

	arrayPageSize = getPageSize();

	var left = ((arrayPageSize[0] - 20 - 100) / 2);

	if (distance < 100)
	{
		scrollTo(0, stopY);
	}

	var speed = Math.round(distance / 100);
	var step  = Math.round(distance / 25);
	var leapY = stopY > startY ? startY + step : startY - step;
	var timer = 0;
	var i;
	var curPos = new Array();

	if (startY > stopY)
	{
		for (i = startY; i > stopY; i -= step)
		{
			setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);

			if (leapY <= stopY)
			{
				setTimeout("showLoader()", timer * speed);
			}

			leapY -= step;

			if (leapY < stopY)
			{
				leapY = stopY;
				timer++;
			}
		}
	}
	else
	{
		speed = Math.round(distance / 10000);
		for (i = startY; i < stopY; i += step)
		{
			setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);

			leapY += step;

			if (leapY > stopY)
			{
				leapY = stopY;
				timer++;
			}
		}
	}
	return false;
}

function elmYPosition(id)
{
	var elm = document.getElementById(id);
	var y = elm.offsetTop;
	var node = elm;
	while (node.offsetParent && node.offsetParent != document.body)
	{
		node = node.offsetParent;
		y += node.offsetTop;
	}
	return y;
}

function currentYPosition()
{
	if (self.pageYOffset)
	{
		return self.pageYOffset;
	}

	if (document.documentElement && document.documentElement.scrollTop)
	{
		return document.documentElement.scrollTop;
	}

	if (document.body.scrollTop)
	{
		return document.body.scrollTop;
	}

	return 0;
}

// Get URL argument
function getArg(name)
{
	name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	var regexS = "[\\?&]"+name+"=([^&#]*)";
	var regex = new RegExp(regexS);
	var results = regex.exec(window.location.href);
	if (results == null)
	{
		return "";
	}
	else
	{
		return results[1];
	}
}

// If get Not-a-Number (NaN) value, return 0
function getIntVal(arg)
{
	arg = parseInt(arg);

	if (isNaN(arg))
	{
		return 0;
	}
	else
	{
		return arg;
	}
}

// Url encoding
var Url = {

	// public method for url encoding
	encode : function (string)
	{
		return escape(this._utf8_encode(string));
	},

	// public method for url decoding
	decode : function (string)
	{
		return this._utf8_decode(unescape(string));
	},

	// private method for UTF-8 encoding
	_utf8_encode : function (string)
	{
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";

		for (var n = 0; n < string.length; n++)
		{
			var c = string.charCodeAt(n);

			if (c < 128)
			{
				utftext += String.fromCharCode(c);
			}
			else if ((c > 127) && (c < 2048))
			{
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else
			{
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}

		}

		return utftext;
	},

	// private method for UTF-8 decoding
	_utf8_decode : function (utftext)
	{
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;

		while (i < utftext.length)
		{
			c = utftext.charCodeAt(i);

			if (c < 128)
			{
				string += String.fromCharCode(c);
				i++;
			}
			else if ((c > 191) && (c < 224))
			{
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else
			{
				c2 = utftext.charCodeAt(i+1);
				c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}

		}

		return string;
	}
}

// Function to store a cookie
function setCookie(c_name,value,expiredays)
{
	var exdate = new Date();
	exdate.setDate (exdate.getDate() + expiredays);
	document.cookie = c_name + "=" + escape(value) + ((expiredays==null) ? "" : ";expires=" + exdate.toGMTString());
}

function trim(s)
{

	return rtrim(ltrim(s)).replace(/%20/g, '');
}

function ltrim(s)
{
	var l = 0;

	while (l < s.length && s[l] == ' ')
	{
		l++;
	}

	return s.substring(l, s.length);
}

function rtrim(s)
{
	var r = s.length -1;

	while (r > 0 && s[r] == ' ')
	{
		r-=1;
	}

	return s.substring(0, r + 1);
}
/**
*
* @package tagcloud_phpbb3
* @version $Id: tagcloud.js 895 2009-06-18 09:52 sayakb $
* @copyright (c) 2005 phpBB Group
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

var xmlhttp, gtid;

function showTagEditor ()
{
	document.getElementById('tageditor').style.display = 'inline';
	document.getElementById('clearlink_u').style.display = 'inline';
	document.getElementById('cancellink_u').style.display = 'inline';
	document.getElementById('searchlink').style.display = 'none';
	document.getElementById('taglist').style.display = 'none';
	document.getElementById('editlink').style.display = 'none';

	var allTags = document.getElementById('taglistclean').innerHTML;
	allTags = allTags.replace(/\+/g, " ");
	allTags = allTags.replace('<i>None</i>', "");
	document.getElementById('tagbox').value = allTags;
	document.getElementById('tagbox').focus();
	document.getElementById('oldtags').innerHTML = allTags;

	return false;
}

function hideTagEditor ()
{
	document.getElementById('editlink').style.display = 'inline';
	document.getElementById('searchlink').style.display = 'inline';
	document.getElementById('clearlink_u').style.display = 'none';
	document.getElementById('tageditor').style.display = 'none';
	document.getElementById('cancellink_u').style.display = 'none';
	document.getElementById('taglist').style.display = 'block';
	document.getElementById('notice_spaces').style.display = 'inline';
	document.getElementById('tag_spinner').style.display = 'none';

	return false;
}

function updateTags( topic_id, sid )
{
	var boxval = document.getElementById('tagbox').value;
	var old = document.getElementById('oldtags').innerHTML;
	var url = "viewtopic.php?ajax=1&t=" + topic_id + "&action=updatetags&mytags=" + boxval + "&oldtags=" + old + "&sid=" + sid;
	
	gtid = topic_id;

	var utlist = boxval;
	utlist = utlist.replace(/, /g, ",");
	utlist = utlist.replace(/,/g, ", ");
	document.getElementById('taglistclean').innerHTML = utlist;
	
	document.getElementById('notice_spaces').style.display = 'none';
	document.getElementById('tag_spinner').style.display = 'inline';
	xmlhttp = GetXmlHttpObject();
	xmlhttp.onreadystatechange = upList;
	xmlhttp.open("GET", url, true);
	xmlhttp.send(null);
	return false;
}

function delTags( topic_id, sid )
{
	var url = "viewtopic.php?ajax=1&t=" + topic_id + "&action=updatetags&delete=1" + "&sid=" + sid;
	gtid = topic_id;
	document.getElementById('notice_spaces').style.display = 'none';
	document.getElementById('tag_spinner').style.display = 'inline';
	document.getElementById('taglistclean').innerHTML = '';

	xmlhttp = GetXmlHttpObject();
	xmlhttp.onreadystatechange = upList;
	xmlhttp.open("GET", url, true);
	xmlhttp.send(null);

	return false;
}

function upList()
{
	if ( xmlhttp.responseText )
	{
		var update = document.getElementById('tagbox').value;
		document.getElementById('oldtags').innerHTML = update;
		document.getElementById('taglist').innerHTML = xmlhttp.responseText;
		hideTagEditor(gtid);
	}
}

function GetXmlHttpObject()
{
	if ( window.XMLHttpRequest ) return new XMLHttpRequest();
	if ( window.ActiveXObject ) return new ActiveXObject("Microsoft.XMLHTTP");
	return null;
}

function ltrim ( str , chars )
{
	chars = chars || "\\s";
	return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}

function showSearchBox()
{
	document.getElementById('searchbox').style.display = 'inline';
	document.getElementById('cancellink_s').style.display = 'inline';
	document.getElementById('editlink').style.display = 'none';
	document.getElementById('searchlink').style.display = 'none';
	document.getElementById('searcharea').value = '';
	document.getElementById('searcharea').focus();

	return false;
}

function hideSearchBox()
{
	document.getElementById('searchbox').style.display = 'none';
	document.getElementById('cancellink_s').style.display = 'none';
	document.getElementById('editlink').style.display = 'inline';
	document.getElementById('searchlink').style.display = 'inline';

	return false;
}
/**
*
* @package kde_brainstorm_phpBB3
* @version $Id: kb_noajax_uncompressed.js sayakb $
* @copyright (c) 2010 Sayak Banerjee
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

var hash = window.location.hash.replace('#', '');
var hashArr = new Array();
var url = "brainstorm.php", anchor = "#anchor";

hashArr = hash.split('_');

for (var i = 0; i < hashArr.length; i++)
{
	if (hashArr[i].substr(0, 4) == 'idea')
	{
		addToUrl('mode', 'idea');
		addToUrl('i', hashArr[i].replace('idea', ''));
	}
	else if (hashArr[i].substr(0, 3) == 'cat')
	{
		addToUrl('mode', 'main');
		addToUrl('f', hashArr[i].replace('cat', ''));
	}
	else if (hashArr[i].substr(0, 7) == 'comment')
	{
		addToUrl('c', hashArr[i].replace('comment', ''));
	}
	else if (hashArr[i].substr(0, 4) == 'page')
	{
		addToUrl('page', hashArr[i].replace('page', ''));
	}
	else if (hashArr[i].substr(0, 6) == 'jumpto')
	{
		anchor += hashArr[i].replace('jumpto', '');
	}
	else if (hashArr[i].length > 0 && hashArr[i].substr(0, 6) != 'anchor')
	{
		addToUrl('mode', hashArr[i]);
	}
}

if (url != 'brainstorm.php')
{
	window.location = url + (anchor != '#anchor' ? anchor : '');
}

function addToUrl(param, value)
{
	var sep;
	
	if (url.indexOf('?') == -1)
	{
		sep = "?";
	}
	else
	{
		sep = "&";
	}
	
	url = url + sep + param + '=' + value;
}
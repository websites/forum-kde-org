/**
*
* @package kdialogue_phpBB3
* @version $Id: kdialogue.js 1003 2009-09-03 23:47 sayakb $
* @copyright (c) 2009 Sayak Banerjee
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

function votePost ( post_id, vote )
{
	$("#post_vote_" + post_id).fadeOut("slow", function(){
		$("#post_vote_" + post_id).css('color', '#000000');
		$("#post_wait_" + post_id).fadeIn("slow", function() {
			$("#post_voteup_e_" + post_id).hide();
			$("#post_voteup_d_" + post_id).show();
			$("#post_votedn_e_" + post_id).hide();
			$("#post_votedn_d_" + post_id).show();

			$.get ("viewtopic.php", { p: post_id, v: vote, ajax: "1", sid: getArg("sid") },
			function ( count ){
				if ( count && !isNaN(count) )
				{
					document.getElementById('post_vote_' + post_id).innerHTML = count;
				}

				$("#post_wait_" + post_id).fadeOut("slow",
					function(){
					$("#post_vote_" + post_id).fadeIn("slow");
				});
			});
		});
	});
	
	return false;
}

// Get URL argument
function getArg( name )
{
	name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	var regexS = "[\\?&]"+name+"=([^&#]*)";
	var regex = new RegExp( regexS );
	var results = regex.exec( window.location.href );
	if( results == null )
	{
		return "";
	}
	else
	{
		return results[1];
	}
}
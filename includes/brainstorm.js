/**
*
* @package javascript_phpBB3
* @version $Id: brainstorm.js sayakb $
* @copyright (c) 2012 Sayak Banerjee
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

// Initialize brainstorm functions
function brainstormStartup() {
	try {
		// Parse old brainstorm links
		fixOldLinks();

		// Set button click event. We don't support JS for KHTML, sorry
		if (!is_khtml) {
			$("#kb-button-up, #kb-button-dn").click(function() {
				var vote = $(this).attr("data-vote");

				if (vote == "up" || vote == "dn") {
					// First disable the buttons, we don't want multiple clicks
					$("#kb-button-up, #kb-button-dn").attr("disabled", "disabled");

					// Generate an overlay
					var top = $("#kb-container").offset().top;
					var left = $("#kb-container").offset().left;
					var height = $("#kb-container").outerHeight();
					var width = $("#kb-container").outerWidth();
					var radius = $('#kb-container').css('borderRadius');
					var overlay = getOverlay(top, left, height, width, radius);

					$("body").append(overlay);
					$("#kb-overlay").fadeIn();

					// Get the post action
					var action = $("#kb-container").attr("action");

					// Send an async request to the server
					$.post(action, { voteajax: vote }, function(data) {
						var votes = JSON.parse(data);

						// Hide the overlay
						$("#kb-overlay").fadeOut(function() {
							$(this).remove();
						});

						// Calculate votes and percentages
						var effective = votes.up - votes.dn;
						var total = votes.up + votes.dn;
						var up_pct = (votes.up / total) * 100;
						var dn_pct = (votes.dn / total) * 100;

						// Update the data on the UI
						$("#kb-votes-effective").html(effective);
						$("#kb-votes-up").html(votes.up);
						$("#kb-votes-dn").html(votes.dn);
						$("#kb-votes-up-pct").css("width", up_pct + "%");
						$("#kb-votes-dn-pct").css("width", dn_pct + "%");

						// Re-enable the vote buttons based on what was voted
						if (vote == "up") {
							$("#kb-button-dn").removeAttr("disabled");
						} else {
							$("#kb-button-up").removeAttr("disabled");
						}

						// Show off a bit!
						$("#kb-votes-up-prg, #kb-votes-dn-prg").addClass("active");

						setTimeout(function() {
							$("#kb-votes-up-prg, #kb-votes-dn-prg").removeClass("active");
						}, 2000);
					});
				}

				return false;
			});
		}
	} catch(e) {
		console.log("Error: " + e.message);
		return false;
	}
}

// Fix old KDE Brainstorm links
function fixOldLinks() {
	var viewforum = "viewforum.php?sid=" + session_id;
	var viewtopic = "viewtopic.php?sid=" + session_id;
	var url = "viewforum.php?sid=" + session_id + "&f=" + default_redir;
	var hash = window.location.hash.replace("#", "");
	var hash_ary = hash.split("_");
	var id, page;

	// Build the target URL
	if (window.location.toString().indexOf("brainstorm.php") != -1) {
		for (i in hash_ary) {
			if (hash_ary[i].substr(0, 4) == "idea") {
				url = (viewtopic + "&t=" + hash_ary[i].replace("idea", ""));
			}

			if (hash_ary[i].substr(0, 3) == "cat") {
				url = (viewforum + "&f=" + hash_ary[i].replace("cat", ""));
			}

			if (hash_ary[i].substr(0, 7) == "comment") {
				id = hash_ary[i].replace("comment", "");
				url += ("&p=" + id + "#p" + id);
			}

			if (hash_ary[i].substr(0, 4) == "page") {
				page = parseInt(hash_ary[i].replace("page", ""));

				if (!isNaN(page)) {
					url += ("&start=" + (page * per_page));
				}
			}
		}

		// Redirect to the URL
		window.location = url;
	}
}

function getOverlay(top, left, height, width, radius) {
	var offset = (height / 2) - 18;

	return '<div id="kb-overlay" style="position:absolute; left:' + left +
		   'px; top:' + top + 'px; height:' + height + 'px; width:' + width +
		   'px; background:rgba(0,0,0,0.15); border-radius:' + radius +';' +
		   'text-align:center; display:none;"><button class="btn" disabled ' +
		   'style="margin-top:' + offset + 'px"><i class="icon-time ' +
		   'icon-black"></i>&nbsp;' + lang_loading + '</button></div>';
}

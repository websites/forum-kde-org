var xmlhttp1, xmlhttp2;

function giveKarma(uid, key, sid)
{
	xmlhttp1 = GetXmlHttpObject();
	var url = "xmlhttp.php?mode=karmapad&u=" + uid + "&key=" + key + "&sid=" + sid;
	setCookie('requestkey', key, 1000);
	
	xmlhttp1.onreadystatechange = padShow;
	xmlhttp1.open("GET",url,true);
	xmlhttp1.send(null);
	
	if ( !document.getElementById('karma_form').innerHTML )
	{
		document.getElementById('spinner').style.display = 'inline';
	}
	return false;
}

function modifyKarma(mode, rid, uid, key, sid)
{
	xmlhttp2 = GetXmlHttpObject();

	if ( mode == 'delete' )
	{
		var delr = confirm('Are you sure?');
		if ( delr != true ) return false;
	}
	
	var url;
	url = "xmlhttp.php?mode=modifykarma&a=" + mode + "&r=" + rid + "&u=" + uid + "&key=" + key + "&sid=" + sid;

	if ( mode != 'delete' )
	{
		urate = document.getElementById('rating').value;
		ucomm = document.getElementById('comment').value;
		setCookie('karma_rating',urate,1000);
		setCookie('karma_comment',ucomm,1000);
	}
	setCookie('requestkey', key, 1000);

	xmlhttp2.onreadystatechange = populateList;
	xmlhttp2.open("GET",url,true);
	xmlhttp2.send(null);

	document.getElementById('spinner').style.display = 'inline';

	return false;
}

function padShow()
{
	if ( xmlhttp1.responseText && !document.getElementById('karma_form').innerHTML )
	{
		document.getElementById('karma_form').innerHTML = xmlhttp1.responseText;
		document.getElementById('spinner').style.display = 'none';
	}
}

function populateList()
{
	if ( xmlhttp2.responseText )
	{
        location.reload(true);
	}
}

function cancelKarma()
{
	document.getElementById('karma_form').innerHTML = "";
}

function GetXmlHttpObject()
{
	if (window.XMLHttpRequest) return new XMLHttpRequest();
	if (window.ActiveXObject) return new ActiveXObject("Microsoft.XMLHTTP");
	return null;
}

function setCookie(c_name,value,expiredays)
{
	var exdate = new Date();
	exdate.setDate (exdate.getDate() + expiredays);
	document.cookie = c_name + "=" + escape(value) +
	((expiredays==null) ? "" : ";expires=" + exdate.toGMTString());
}

function getCookie(c_name)
{
	if (document.cookie.length>0)
	{
		c_start=document.cookie.indexOf(c_name + "=");
		if (c_start!=-1)
		{
			c_start=c_start + c_name.length+1;
			c_end=document.cookie.indexOf(";",c_start);
			if (c_end==-1) c_end=document.cookie.length;
			return unescape(document.cookie.substring(c_start,c_end));
		}
	}
	return "";
}
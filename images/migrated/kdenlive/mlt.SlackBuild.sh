#!/bin/sh

#
# Global variables
#

CURDIR=`pwd`
BUILDDIR="${CURDIR}/mlt"
TARGETDIR="${CURDIR}/target"
INSTALLDIR="${TARGETDIR}/usr/"
ARCH=${ARCH:-i686}
BUILD=${BUILD:-1}
TAG=${TAG:-_MDG}
PRGNAME=mlt
VERSION="0.4.4"

main()
{
	makedirs
	uncompress
	build
	make_pkg
#	cleanup
}

download()
{
	echo "Starting download..."
	wget http://downloads.sourceforge.net/project/mlt/mlt/${VERSION}/mlt-${VERSION}.tar.gz?use_mirror=ovh
	echo "Download complete"
}

uncompress()
{
	if [ -e ${PRGNAME}-${VERSION}.tar.gz ]
	then
		echo "Uncompressing..."
		tar zxf ${PRGNAME}-${VERSION}.tar.gz
		if [ -d ${PRGNAME}-${VERSION} ]
		then
			# when downloaded from stable branch
			# the source directory gets a version number
			# it gets stripped here
			mv ${PRGNAME}-${VERSION} ${PRGNAME}
		fi
	else
		download
	fi
}

makedirs()
{
	echo "Creating dirs..."
	mkdir -p ${SRCDIR} ${BUILDDIR} ${TARGETDIR}
}

build()
{
	cd ${BUILDDIR}
	./configure --prefix=${INSTALLDIR} --enable-gpl --qimage-libdir=/usr/lib/ --qimage-includedir=/usr/include/qt4 --avformat-swscale
	if [ $? -eq 0 ]
	then
		make
		make install
	fi
}

cleanup()
{
	echo "Cleaning up"
	cd ${CURDIR}
	rm -rf ${TARGETDIR} ${BUILDDIR} ${PRGNAME}
}

make_pkg()
{
	mkdir -p ${TARGETDIR}/usr/doc/${PRGNAME}-${VERSION}

	cp -a AUTHORS COPYING INSTALL README ${TARGETDIR}/usr/doc/${PRGNAME}-${VERSION} 
	cat ${CURDIR}/${PRGNAME}.SlackBuild > ${TARGETDIR}/usr/doc/${PRGNAME}-${VERSION}/${PRGNAME}.SlackBuild
	find ${TARGETDIR}/usr/doc/$PRGNAM-$VERSION -type f -exec chmod 644 {} \;

	mkdir -p ${TARGETDIR}/install
	cat ${CURDIR}/slack-desc > ${TARGETDIR}/install/slack-desc

	cd ${TARGETDIR}
	/sbin/makepkg -l y -c n ${CURDIR}/${PRGNAME}-${VERSION}-${BUILD}-${ARCH}${TAG}.tgz

}

main

Mon Mar 30 22:18:53 CEST 2009
This is a logfile of running kdenlive through gdb. You may want to visit http://www.kdenlive.org/mantis/view_all_bug_page.php and attach this file to a proper bug
Kdenlive was installed by Kdenlive Builder Wizard 0.7.3
Information about revisions of modules at the time of compilation:
ffmpeg: Revision: 18155 Last Changed Date: 2009-03-22 22:31:23 +0100 (Sun, 22 Mar 2009)
mlt: Revision: 1380 Last Changed Date: 2009-03-17 19:38:54 +0100 (Tue, 17 Mar 2009)
mlt++: Revision: 1380 Last Changed Date: 2009-02-04 16:49:35 +0100 (Wed, 04 Feb 2009)
kdenlive: Revision: 3167 Last Changed Date: 2009-03-20 14:39:34 +0100 (Fri, 20 Mar 2009)
uname -a at time of compilation:
Linux ubuntu 2.6.28-11-generic #37-Ubuntu SMP Mon Mar 23 16:40:00 UTC 2009 x86_64 GNU/Linux
Information about cc at the time of compilation:
Using built-in specs.
Target: x86_64-linux-gnu
Configured with: ../src/configure -v --with-pkgversion='Ubuntu 4.3.3-5ubuntu4' --with-bugurl=file:///usr/share/doc/gcc-4.3/README.Bugs --enable-languages=c,c++,fortran,objc,obj-c++ --prefix=/usr --enable-shared --with-system-zlib --libexecdir=/usr/lib --without-included-gettext --enable-threads=posix --enable-nls --with-gxx-include-dir=/usr/include/c++/4.3 --program-suffix=-4.3 --enable-clocale=gnu --enable-libstdcxx-debug --enable-objc-gc --enable-mpfr --with-tune=generic --enable-checking=release --build=x86_64-linux-gnu --host=x86_64-linux-gnu --target=x86_64-linux-gnu
Thread model: posix
gcc version 4.3.3 (Ubuntu 4.3.3-5ubuntu4) 
/usr/bin/dpkg
Found dpkg - running dpkg -l to grep libc6
ii  libc6                                      2.9-4ubuntu3                      GNU C Library: Shared libraries
ii  libc6-dev                                  2.9-4ubuntu3                      GNU C Library: Development Libraries and Hea
ii  libc6-i386                                 2.9-4ubuntu3                      GNU C Library: 32bit shared libraries for AM
Information about kde 4 at the time of compilation:
Qt: 4.5.0
KDE: 4.2.1 (KDE 4.2.1)
kde4-config: 1.0
uname -a at runtime:
Linux ubuntu 2.6.28-11-generic #37-Ubuntu SMP Mon Mar 23 16:40:00 UTC 2009 x86_64 GNU/Linux
Information about kde 4 at runtime:
Qt: 4.5.0
KDE: 4.2.1 (KDE 4.2.1)
kde4-config: 1.0
Information about kde 3 at runtime:
Qt: 3.3.8b
KDE: 3.5.10
kde-config: 1.0
INSTALL_DIR=/home/cvts/kdenlive
MLT_AVFORMAT_THREADS=2
PATH=/home/cvts/kdenlive/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/lib/kde4/bin:/usr/lib/kde4/bin
LD_LIBRARY_PATH=/home/cvts/kdenlive/lib:
MLT_PATH=/home/cvts/kdenlive
Running ldd /home/cvts/kdenlive/bin/kdenlive
	linux-vdso.so.1 =>  (0x00007fff0e7fe000)
	libmlt.so.1 => /home/cvts/kdenlive/lib/libmlt.so.1 (0x00007f8906249000)
	libmlt++.so.1 => /home/cvts/kdenlive/lib/libmlt++.so.1 (0x00007f8906026000)
	libnepomuk.so.4 => /usr/lib/libnepomuk.so.4 (0x00007f8905db5000)
	libkdeui.so.5 => /usr/lib/libkdeui.so.5 (0x00007f89057c2000)
	libkio.so.5 => /usr/lib/libkio.so.5 (0x00007f890530c000)
	libknewstuff2.so.4 => /usr/lib/libknewstuff2.so.4 (0x00007f89050a7000)
	libknotifyconfig.so.4 => /usr/lib/libknotifyconfig.so.4 (0x00007f8904e94000)
	libQtSvg.so.4 => /usr/lib/libQtSvg.so.4 (0x00007f8904c3b000)
	libQtNetwork.so.4 => /usr/lib/libQtNetwork.so.4 (0x00007f8904917000)
	libQtXml.so.4 => /usr/lib/libQtXml.so.4 (0x00007f89046cf000)
	libkdecore.so.5 => /usr/lib/libkdecore.so.5 (0x00007f890426c000)
	libQtDBus.so.4 => /usr/lib/libQtDBus.so.4 (0x00007f8903ff5000)
	libQtCore.so.4 => /usr/lib/libQtCore.so.4 (0x00007f8903ba8000)
	libQtGui.so.4 => /usr/lib/libQtGui.so.4 (0x00007f8902f6a000)
	libstdc++.so.6 => /usr/lib/libstdc++.so.6 (0x00007f8902c5d000)
	libm.so.6 => /lib/libm.so.6 (0x00007f89029d8000)
	libgcc_s.so.1 => /lib/libgcc_s.so.1 (0x00007f89027c0000)
	libc.so.6 => /lib/libc.so.6 (0x00007f890244e000)
	libdl.so.2 => /lib/libdl.so.2 (0x00007f890224a000)
	libpthread.so.0 => /lib/libpthread.so.0 (0x00007f890202e000)
	libmiracle.so.1 => /home/cvts/kdenlive/lib/libmiracle.so.1 (0x00007f8901e20000)
	libsoprano.so.4 => /usr/lib/libsoprano.so.4 (0x00007f8901b4a000)
	libsopranoclient.so.1 => /usr/lib/libsopranoclient.so.1 (0x00007f8901900000)
	libSM.so.6 => /usr/lib/libSM.so.6 (0x00007f89016f7000)
	libICE.so.6 => /usr/lib/libICE.so.6 (0x00007f89014dc000)
	libX11.so.6 => /usr/lib/libX11.so.6 (0x00007f89011d5000)
	libXext.so.6 => /usr/lib/libXext.so.6 (0x00007f8900fc3000)
	libXft.so.2 => /usr/lib/libXft.so.2 (0x00007f8900dae000)
	libXau.so.6 => /usr/lib/libXau.so.6 (0x00007f8900bab000)
	libXdmcp.so.6 => /usr/lib/libXdmcp.so.6 (0x00007f89009a6000)
	libXtst.so.6 => /usr/lib/libXtst.so.6 (0x00007f890079f000)
	libXcursor.so.1 => /usr/lib/libXcursor.so.1 (0x00007f8900595000)
	libXfixes.so.3 => /usr/lib/libXfixes.so.3 (0x00007f8900390000)
	libXrender.so.1 => /usr/lib/libXrender.so.1 (0x00007f8900186000)
	libz.so.1 => /lib/libz.so.1 (0x00007f88fff6e000)
	libstreamanalyzer.so.0 => /usr/lib/libstreamanalyzer.so.0 (0x00007f88ffcfb000)
	libstreams.so.0 => /usr/lib/libstreams.so.0 (0x00007f88ffac3000)
	libsolid.so.4 => /usr/lib/libsolid.so.4 (0x00007f88ff839000)
	libacl.so.1 => /lib/libacl.so.1 (0x00007f88ff631000)
	libattr.so.1 => /lib/libattr.so.1 (0x00007f88ff42c000)
	libphonon.so.4 => /usr/lib/libphonon.so.4 (0x00007f88ff1dd000)
	libbz2.so.1.0 => /lib/libbz2.so.1.0 (0x00007f88fefcc000)
	libresolv.so.2 => /lib/libresolv.so.2 (0x00007f88fedb4000)
	libgthread-2.0.so.0 => /usr/lib/libgthread-2.0.so.0 (0x00007f88febaf000)
	librt.so.1 => /lib/librt.so.1 (0x00007f88fe9a7000)
	libglib-2.0.so.0 => /usr/lib/libglib-2.0.so.0 (0x00007f88fe6e2000)
	libaudio.so.2 => /usr/lib/libaudio.so.2 (0x00007f88fe4c9000)
	libpng12.so.0 => /usr/lib/libpng12.so.0 (0x00007f88fe2a2000)
	libfreetype.so.6 => /usr/lib/libfreetype.so.6 (0x00007f88fe01c000)
	libgobject-2.0.so.0 => /usr/lib/libgobject-2.0.so.0 (0x00007f88fddd6000)
	libfontconfig.so.1 => /usr/lib/libfontconfig.so.1 (0x00007f88fdba4000)
	/lib64/ld-linux-x86-64.so.2 (0x00007f890646e000)
	libvalerie.so.1 => /home/cvts/kdenlive/lib/libvalerie.so.1 (0x00007f88fd999000)
	libuuid.so.1 => /lib/libuuid.so.1 (0x00007f88fd794000)
	libxcb.so.1 => /usr/lib/libxcb.so.1 (0x00007f88fd578000)
	libxml2.so.2 => /usr/lib/libxml2.so.2 (0x00007f88fd21b000)
	libpcre.so.3 => /lib/libpcre.so.3 (0x00007f88fcfeb000)
	libXt.so.6 => /usr/lib/libXt.so.6 (0x00007f88fcd87000)
	libexpat.so.1 => /usr/lib/libexpat.so.1 (0x00007f88fcb5d000)
Running gdb --args /home/cvts/kdenlive/bin/kdenlive --nocrashhandler --mlt-path /home/cvts/kdenlive
GNU gdb 6.8-debian
Copyright (C) 2008 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "x86_64-linux-gnu"...
(gdb) (gdb) set pagination off
(gdb) (gdb) run
(gdb) Starting program: /home/cvts/kdenlive/bin/kdenlive --nocrashhandler --mlt-path /home/cvts/kdenlive
[Thread debugging using libthread_db enabled]
[New Thread 0x7fe7ac7c7750 (LWP 4431)]
/usr/share/themes/Human/gtk-2.0/gtkrc:82: Murrine configuration option "highlight_ratio" will be deprecated in future releases. Please use "highlight_shade" instead.
/usr/share/themes/Human/gtk-2.0/gtkrc:83: Murrine configuration option "lightborder_ratio" will be deprecated in future releases. Please use "lightborder_shade" instead.
/usr/share/themes/Human/gtk-2.0/gtkrc:194: Murrine configuration option "highlight_ratio" will be deprecated in future releases. Please use "highlight_shade" instead.
kdenlive(4431) MainWindow::parseProfiles: RESULTING MLT PATH:  "/home/cvts/kdenlive/share/mlt/profiles/"
kdenlive(4431) initEffects::parseEffectFiles: //  INIT EFFECT SEARCH
[New Thread 0x7fe79278f950 (LWP 4436)]
[Thread 0x7fe79278f950 (LWP 4436) exited]

Program exited with code 01.
(gdb) bt
(gdb) No stack.
(gdb) thread apply all bt
(gdb) No registers.
(gdb) where full
(gdb) No stack.
(gdb) quit

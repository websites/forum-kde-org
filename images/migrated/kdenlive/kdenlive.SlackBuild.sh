#!/bin/sh

#
# Global variables
#

CURDIR=`pwd`
BUILDDIR="${CURDIR}/build"
TARGETDIR="${CURDIR}/target"
INSTALLDIR="${TARGETDIR}/usr/"
ARCH=${ARCH:-i686}
BUILD=${BUILD:-1}
TAG=${TAG:-_MDG}
PRGNAME=kdenlive
VERSION="0.7.5"

main()
{
	makedirs
	uncompress
	build
	make_pkg
	cleanup
}

download()
{
	echo "Starting download..."
	svn co https://kdenlive.svn.sourceforge.net/svnroot/kdenlive/trunk/kdenlive
	echo "Download complete, compressing"
	tar jcf kdenlive-${VERSION}.tar.bz2 kdenlive
}

uncompress()
{
	if [ -e kdenlive-${VERSION}.tar.bz2 ]
	then
		echo "Uncompressing..."
		tar jxf kdenlive-${VERSION}.tar.bz2
		if [ -d kdenlive-${VERSION} ]
		then
			# when downloaded from stable branch
			# the source directory gets a version number
			# it gets stripped here
			mv kdenlive-${VERSION} kdenlive
		fi
	else
		download
	fi
}

makedirs()
{
	echo "Creating dirs..."
	mkdir -p ${SRCDIR} ${BUILDDIR} ${TARGETDIR}
}

build()
{
	cd ${BUILDDIR}
	cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX=${INSTALLDIR} ../kdenlive/
	make
	make install
}

cleanup()
{
	echo "Cleaning up"
	cd ${CURDIR}
	rm -rf ${TARGETDIR} ${BUILDDIR} kdenlive
}

make_pkg()
{
	mkdir -p ${TARGETDIR}/usr/doc/${PRGNAME}-${VERSION}

	cp -a AUTHORS COPYING INSTALL README ${TARGETDIR}/usr/doc/${PRGNAME}-${VERSION} 
	cat ${CURDIR}/${PRGNAME}.SlackBuild > ${TARGETDIR}/usr/doc/${PRGNAME}-${VERSION}/${PRGNAME}.SlackBuild
	find ${TARGETDIR}/usr/doc/$PRGNAM-$VERSION -type f -exec chmod 644 {} \;

	mkdir -p ${TARGETDIR}/install
	cat ${CURDIR}/slack-desc > ${TARGETDIR}/install/slack-desc

	cd ${TARGETDIR}
	/sbin/makepkg -l y -c n ${CURDIR}/${PRGNAME}-${VERSION}-${BUILD}-${ARCH}${TAG}.tgz

}

main
